package com.itravelapp.damdama.managers;

import com.itravelapp.damdama.models.ExplorePoint;
import com.itravelapp.damdama.models.Placard;
import com.itravelapp.damdama.models.TreasurePoint;

import java.util.Date;
import java.util.Random;
import java.util.Vector;

/**
 * Created by mohit on 1/6/16.
 */
public class DataManager {

    private static DataManager ownInstance = new DataManager();

    public Date gameStartTime = null;
    public int treasuresFound;
    public Vector<TreasurePoint> treasurePointsList;
    public Vector<Integer> completedList;

    public Vector<Placard> explorePlacards = null;
    public Placard selectedPlacard = null;
    public ExplorePoint selectedExplorePoint = null;
    public Vector<String> qrCodeInfo = null;

    public int screenWidth = 0;

    public long maxGameTime = 2700000;

    private DataManager() {

    }

    public static DataManager getInstance() {
        return ownInstance;
    }


    /*

 - $ will become &#S2958$
 - Each alphabetic character will become 2 digit number
 - A = 20 + 01 = 21
 - Z = 20 + 26 = 46
 - , = 20 + 27 = 47
 - . = 20 + 28 = 48
 - hipen = 20 + 29 = 49
 - (space) = 20 + 30 = 50
 - Each single digit will digit will become 2 digit number
 - 0 = 10 + 0 = 10
 - 9 = 10 + 9 = 19

 */

    public static Vector<String> decodeQRCodeData(String encodedString){

        Vector<String> decodedStings = new Vector<String>();

        if(encodedString == null || encodedString.length() == 0)
        return decodedStings;

        String separator = "\\&\\#S2958\\$";
        String[] encodedStringsList = encodedString.split(separator);

        System.out.println("length "+encodedStringsList.length);

        for (int i = 0; i < encodedStringsList.length; i++) {

            String str = encodedStringsList[i];

            if(str.length() > 0)
                decodedStings.addElement(decodeText(str));
        }

        return decodedStings;
    }

    public Vector<String> splitText(String string, String separator){

        Vector<String> components = new Vector<String>();

        if(string != null || separator != null || string.length() > 0 && separator.length() > 0){

            Boolean hasCharcters = false;
            String str = string;
            int separatorLength = separator.length();
            while (hasCharcters == false){
                int startIndex = str.indexOf(separator);


                hasCharcters = true;

            }


        }

        return components;
    }

    public static String decodeText(String encodedText){

        String decodedString = "";

        for(int i = 0; i < encodedText.length(); i+=2){

            String partString = encodedText.substring(i,(i+2));
            int encodedNumber = Integer.parseInt(partString);

            if(encodedNumber >= 10 && encodedNumber <= 19){
                decodedString += (encodedNumber - 10);
            }else if(encodedNumber >= 20 && encodedNumber <= 46){
                int asciiCode = 64 + (encodedNumber - 20); //65 = A = 64 + (21 % 20);
                char asciiChar = (char)asciiCode;
                decodedString += asciiChar;
            }else if(encodedNumber == 47){
                decodedString += ",";
            }else if(encodedNumber == 48){
                decodedString += ".";
            }else if(encodedNumber == 49){
                decodedString += "-";
            }else if(encodedNumber == 50){
                decodedString += " ";
            }else if(encodedNumber == 51){
                decodedString += "_";
            }

        }

        return decodedString;
    }

    /*

    + (NSArray*)encodeQRCodeData:(NSString*)decodedString{

        if(decodedString == nil || [decodedString length] == 0)
        return [NSArray array];

        NSMutableArray* encodedStings = [NSMutableArray array];

        NSString* separator = @"&#S2958$";

        NSArray* encodedStringsList = [decodedString componentsSeparatedByString:separator];

        for (int i = 0; i < [encodedStringsList count]; i++) {

            NSString* str = [encodedStringsList objectAtIndex:i];
            if([str length] > 0)
            [encodedStings addObject:[Utility decodeText:str]];
        }

        return encodedStings;
    }

    + (NSString*)encodeText:(NSString*)decodedText{

        NSMutableString* encodedString = [NSMutableString string];

        for (int i = 0; i < [decodedText length]; i++) {

            char decodedChar = [decodedText characterAtIndex:i];
            int decodedInt = (int)decodedChar;
            int encodedNumber = 99;

            //48 - 57
            if(decodedInt >= 48 && decodedInt <= 57){
                encodedNumber = 10 + (decodedInt - 48);
            }else if(decodedInt >= 65 && decodedInt <= 90){
                encodedNumber = 20 + (decodedInt - 64);
            }else if(decodedInt >= 97 && decodedInt <= 122){
                encodedNumber = 20 + (decodedInt - 96);
            }else if(decodedInt == 95){ // "_"
                encodedNumber = 51;
            }else if(decodedInt == 32){ // " "
                encodedNumber = 50;
            }else if(decodedInt == 45){ // "-"
                encodedNumber = 49;
            }else if(decodedInt == 46){ // "."
                encodedNumber = 48;
            }else if(decodedInt == 44){ // ","
                encodedNumber = 47;
            }


//        if(encodedNumber >= 10 && encodedNumber <= 19){
//            [decodedString appendFormat:@"%d",(encodedNumber - 10)];
//        }else if(encodedNumber >= 20 && encodedNumber <= 46){
//            int asciiCode = 64 + (encodedNumber - 20); //65 = A = 64 + (21 % 20);
//            [decodedString appendFormat:@"%c",asciiCode];
//        }else if(encodedNumber == 47){
//            [decodedString appendString:@","];
//        }else if(encodedNumber == 48){
//            [decodedString appendString:@"."];
//        }else if(encodedNumber == 49){
//            [decodedString appendString:@"-"];
//        }else if(encodedNumber == 50){
//            [decodedString appendString:@" "];
//        }
//
            [encodedString appendFormat:@"%d",encodedNumber];
        }

        return encodedString;
    }



    + (int)getRandomNumberBetween:(int)from to:(int)to {

        return (int)from + arc4random() % (to-from+1);
    }

    + (NSArray*)generateRandomUniqueNumberInRange:(int)rangeLow rangeHigh:(int)rangeHigh count:(int)count{

        NSMutableArray *unqArray = [[NSMutableArray alloc] init];

        int randNum = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
        int counter = 0;

        while (counter < count ) {// rangeHigh-rangeLow

            if (![unqArray containsObject:[NSNumber numberWithInt:randNum]]) {
                [unqArray addObject:[NSNumber numberWithInt:randNum]];
                counter++;
            }

            randNum = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
        }

        return [NSArray arrayWithArray:unqArray];
    }
*/

    public static int getRandomNumberBetween(int from, int to){

        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((to - from) + 1) + from;

        return randomNum;
    }

    public static Vector<Integer> generateRandomUniqueNumberInRange(int rangeLow , int rangeHigh ,int count){

        Vector<Integer> unqArray = new Vector<Integer>();

        int randNum = getRandomNumberBetween(rangeLow, rangeHigh);
        int counter = 0;

        int numberOfTries = 0;

        while (counter < count ) {// rangeHigh-rangeLow

            if (!unqArray.contains(randNum)) {
                unqArray.add(randNum);
                counter++;
                numberOfTries = 0;
            }

            randNum = getRandomNumberBetween(rangeLow, rangeHigh);
            numberOfTries++;

            if(numberOfTries >= 100){
                unqArray.add(rangeLow);
                counter++;
                numberOfTries = 0;
            }
        }

        return  unqArray;
    }


}
