package com.itravelapp.damdama.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final public class MVCDBModel {

    //Database Name
    private static final String DATABASE_NAME = "aTreeDetails";
    private static final int DATABASE_VERSION = 1;
    //Table Name
    private static final String TREE_TABLE = "tree";
    //Table Creation
    private static final String TREE_TABLE_CREATE_QUERY = "create table " + TREE_TABLE + " ( " + DBConstant.getTreeId() + " TEXT PRIMARY KEY not null,"
            + DBConstant.getTreeName() + " TEXT,"
            + DBConstant.getTreeLocalName() + " TEXT,"
            + DBConstant.getTreeBotanicalName() + " TEXT,"
            + DBConstant.getTreeGridImage() + " TEXT,"
            + DBConstant.getTreeQrCode() + " TEXT,"
            + DBConstant.getTreeImagesJo() + " TEXT,"
            + DBConstant.getLeafImagesJo() + " TEXT,"
            + DBConstant.getFruitImagesJo() + " TEXT,"
            + DBConstant.getFlowerImagesJo() + " TEXT,"
            + DBConstant.getBarkImagesJo() + " TEXT,"
            + DBConstant.getTreeWhichYouLike() + " TEXT)";

    private SQLiteDatabase database;
    private final SQLiteOpenHelper helper;

    MVCDBModel(final Context ctx) {
        this.helper = new SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION) {
            @Override
            public void onCreate(final SQLiteDatabase db) {
                db.execSQL(TREE_TABLE_CREATE_QUERY);
            }

            @Override
            public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + TREE_TABLE_CREATE_QUERY);
                this.onCreate(db);
            }
        };
        this.database = this.helper.getWritableDatabase();
    }

    long addTree(ContentValues data) {
        this.database = this.helper.getWritableDatabase();
        return this.database.insert(TREE_TABLE, null, data);
    }

    Cursor getAllTreeList() {
        this.database = this.helper.getReadableDatabase();
        String columns[] = new String[]{DBConstant.getTreeId(), DBConstant.getTreeName(),DBConstant.getTreeLocalName(),DBConstant.getTreeBotanicalName(), DBConstant.getTreeGridImage(), DBConstant.getTreeQrCode(), DBConstant.getTreeImagesJo(),DBConstant.getLeafImagesJo(),DBConstant.getFruitImagesJo(),DBConstant.getFlowerImagesJo(),DBConstant.getBarkImagesJo(),DBConstant.getTreeWhichYouLike()};
        return this.database.query(TREE_TABLE, columns, null, null, null, null, null);
    }

    Cursor getTreeListFromQrScan(String unique_identifier) {
        this.database = this.helper.getReadableDatabase();
        String selectionArgs[] = new String[]{unique_identifier};
        String columns[] = new String[]{DBConstant.getTreeId(), DBConstant.getTreeName(),DBConstant.getTreeLocalName(),DBConstant.getTreeBotanicalName(), DBConstant.getTreeGridImage(), DBConstant.getTreeQrCode(), DBConstant.getTreeImagesJo(),DBConstant.getLeafImagesJo(),DBConstant.getFruitImagesJo(),DBConstant.getFlowerImagesJo(),DBConstant.getBarkImagesJo(),DBConstant.getTreeWhichYouLike()};
        return this.database.query(TREE_TABLE, columns, DBConstant.getTreeQrCode()+"=?", selectionArgs, null, null, null);
    }

    Cursor getTreeFromId(String treeId) {
        this.database = this.helper.getReadableDatabase();
        String selectionArgs[] = new String[]{treeId};
        String columns[] = new String[]{DBConstant.getTreeId(), DBConstant.getTreeName(),DBConstant.getTreeLocalName(),DBConstant.getTreeBotanicalName(), DBConstant.getTreeGridImage(), DBConstant.getTreeQrCode(), DBConstant.getTreeImagesJo(),DBConstant.getLeafImagesJo(),DBConstant.getFruitImagesJo(),DBConstant.getFlowerImagesJo(),DBConstant.getBarkImagesJo(),DBConstant.getTreeWhichYouLike()};
        return this.database.query(TREE_TABLE, columns, DBConstant.getTreeId()+"=?", selectionArgs, null, null, null);
    }

    Cursor isTreeAlreadyExist(final String tree_id) {
        this.database = this.helper.getWritableDatabase();
        String selectionArgs[] = new String[]{tree_id};
        return this.database.rawQuery("select * from " + TREE_TABLE + " where " + DBConstant.getTreeId() + "=?", selectionArgs);
    }

    Cursor isHasAnyTree() {
        this.database = this.helper.getReadableDatabase();
        return this.database.rawQuery("select * from " + TREE_TABLE, null);
    }

    int deleteTree() {
        this.database = this.helper.getWritableDatabase();
        return this.database.delete(TREE_TABLE, null, null);
    }

}
