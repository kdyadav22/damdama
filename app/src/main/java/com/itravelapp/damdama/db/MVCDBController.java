package com.itravelapp.damdama.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.itravelapp.damdama.models.TreeListModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by kapilyadav on 06-Sep-17.
 */

public class MVCDBController {
    private MVCDBModel model;

    public MVCDBController(final Context context) {
        model = new MVCDBModel(context);
    }

    public long addTree(TreeListModel treeListModel) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstant.getTreeId(), treeListModel.getTreeId());
        contentValues.put(DBConstant.getTreeName(), treeListModel.getTreeName());
        contentValues.put(DBConstant.getTreeLocalName(), treeListModel.getLocalName());
        contentValues.put(DBConstant.getTreeBotanicalName(), treeListModel.getBotanicalName());
        contentValues.put(DBConstant.getTreeGridImage(), treeListModel.getTreeGridImageUrl());
        contentValues.put(DBConstant.getTreeQrCode(), treeListModel.getTreeQRCode());

        contentValues.put(DBConstant.getTreeImagesJo(), treeListModel.getTreesArray());
        contentValues.put(DBConstant.getLeafImagesJo(), treeListModel.getLeafArray());
        contentValues.put(DBConstant.getFruitImagesJo(), treeListModel.getFruitArray());
        contentValues.put(DBConstant.getFlowerImagesJo(), treeListModel.getFlowerArray());
        contentValues.put(DBConstant.getBarkImagesJo(), treeListModel.getBarkArray());

        contentValues.put(DBConstant.getTreeWhichYouLike(), treeListModel.getWhichYouLike());

        return model.addTree(contentValues);
    }

    public ArrayList<TreeListModel> getAllTreeList() {
        ArrayList<TreeListModel> arrayList = new ArrayList<>();
        Cursor cursor = model.getAllTreeList();
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    TreeListModel treeListModel = new TreeListModel();

                    treeListModel.setTreeId(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeId())));
                    treeListModel.setTreeName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeName())));
                    treeListModel.setLocalName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeLocalName())));
                    treeListModel.setBotanicalName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeBotanicalName())));
                    treeListModel.setTreeGridImageUrl(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeGridImage())));
                    treeListModel.setTreesArray(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeImagesJo())));
                    treeListModel.setLeafArray(cursor.getString(cursor.getColumnIndex(DBConstant.getLeafImagesJo())));
                    treeListModel.setFruitArray(cursor.getString(cursor.getColumnIndex(DBConstant.getFruitImagesJo())));
                    treeListModel.setFlowerArray(cursor.getString(cursor.getColumnIndex(DBConstant.getFlowerImagesJo())));
                    treeListModel.setBarkArray(cursor.getString(cursor.getColumnIndex(DBConstant.getBarkImagesJo())));
                    treeListModel.setWhichYouLike(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeWhichYouLike())));
                    treeListModel.setTreeQRCode(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeQrCode())));

                    arrayList.add(treeListModel);

                    Collections.sort(arrayList, new Comparator<TreeListModel>() {
                        @Override
                        public int compare(TreeListModel s1, TreeListModel s2) {
                            return s1.getTreeName().compareToIgnoreCase(s2.getTreeName());
                        }
                    });
                }
            } finally {
                cursor.close();
            }
        }
        return arrayList;
    }

    public TreeListModel getTreeListFromQrScan(String unique_identifier) {
        TreeListModel treeListModel = new TreeListModel();
        Cursor cursor = model.getTreeListFromQrScan(unique_identifier);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    treeListModel.setTreeId(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeId())));
                    treeListModel.setTreeName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeName())));
                    treeListModel.setTreeGridImageUrl(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeGridImage())));
                    treeListModel.setTreesArray(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeImagesJo())));
                    treeListModel.setLeafArray(cursor.getString(cursor.getColumnIndex(DBConstant.getLeafImagesJo())));
                    treeListModel.setFruitArray(cursor.getString(cursor.getColumnIndex(DBConstant.getFruitImagesJo())));
                    treeListModel.setFlowerArray(cursor.getString(cursor.getColumnIndex(DBConstant.getFlowerImagesJo())));
                    treeListModel.setBarkArray(cursor.getString(cursor.getColumnIndex(DBConstant.getBarkImagesJo())));
                    treeListModel.setWhichYouLike(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeWhichYouLike())));
                    treeListModel.setTreeQRCode(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeQrCode())));
                }
            } finally {
                cursor.close();
            }
        }
        return treeListModel;
    }

    public TreeListModel getTreeFromId(String treeId) {
        TreeListModel treeListModel = new TreeListModel();
        Cursor cursor = model.getTreeFromId(treeId);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    treeListModel.setTreeId(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeId())));
                    treeListModel.setTreeName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeName())));
                    treeListModel.setLocalName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeLocalName())));
                    treeListModel.setBotanicalName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeBotanicalName())));
                    treeListModel.setTreeGridImageUrl(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeGridImage())));
                    treeListModel.setTreesArray(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeImagesJo())));
                    treeListModel.setLeafArray(cursor.getString(cursor.getColumnIndex(DBConstant.getLeafImagesJo())));
                    treeListModel.setFruitArray(cursor.getString(cursor.getColumnIndex(DBConstant.getFruitImagesJo())));
                    treeListModel.setFlowerArray(cursor.getString(cursor.getColumnIndex(DBConstant.getFlowerImagesJo())));
                    treeListModel.setBarkArray(cursor.getString(cursor.getColumnIndex(DBConstant.getBarkImagesJo())));
                    treeListModel.setWhichYouLike(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeWhichYouLike())));
                    treeListModel.setTreeQRCode(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeQrCode())));
                }
            } finally {
                cursor.close();
            }
        }
        return treeListModel;
    }

    public TreeListModel getTreeTriviaFromId(String treeId) {
        TreeListModel treeListModel = new TreeListModel();
        Cursor cursor = model.getTreeFromId(treeId);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    treeListModel.setTreeId(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeId())));
                    treeListModel.setLocalName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeLocalName())));
                    treeListModel.setBotanicalName(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeBotanicalName())));
                    treeListModel.setWhichYouLike(cursor.getString(cursor.getColumnIndex(DBConstant.getTreeWhichYouLike())));
                }
            } finally {
                cursor.close();
            }
        }
        return treeListModel;
    }

    public String getTreeId(String unique_identifier) {
        String treeListModel = "";
        Cursor cursor = model.getTreeListFromQrScan(unique_identifier);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    treeListModel = cursor.getString(cursor.getColumnIndex(DBConstant.getTreeId()));
                }
            } finally {
                cursor.close();
            }
        }
        return treeListModel;
    }

    public String getTreeName(String unique_identifier) {
        String treeListModel = "";
        Cursor cursor = model.getTreeListFromQrScan(unique_identifier);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    treeListModel = cursor.getString(cursor.getColumnIndex(DBConstant.getTreeName()));
                }
            } finally {
                cursor.close();
            }
        }
        return treeListModel;
    }

    public int deleteTree() {
        return model.deleteTree();
    }

    public boolean isHasAnyTree() {
        Cursor cursor = model.isHasAnyTree();
        try{
            if (cursor.getCount() > 0)
                return true;
            else return false;
        }finally {
            cursor.close();
        }
    }

    public boolean isTreeAlreadyExist(final String tree_id) {
        Cursor cursor = model.isTreeAlreadyExist(tree_id);
        try{
            if (cursor.getCount() > 0)
                return true;
            else return false;
        }finally {
            cursor.close();
        }
    }

}
