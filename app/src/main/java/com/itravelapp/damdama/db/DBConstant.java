package com.itravelapp.damdama.db;

/**
 * Created by kapilyadav on 06-Sep-17.
 */

class DBConstant {
    static String getDatabaseName() {
        return DATABASE_NAME;
    }

    static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    static String getTreeTable() {
        return TREE_TABLE;
    }

    static String getTreeId() {
        return TREE_ID;
    }

    static String getTreeName() {
        return TREE_NAME;
    }

    static String getTreeImagesJo() {
        return TREE_IMAGES_JO;
    }

    static String getTreeGridImage() {
        return TREE_GRID_IMAGE;
    }

    static String getTreeQrCode() {
        return TREE_QR_CODE;
    }

    static String getTreeWhichYouLike() {
        return TREE_WHICH_YOU_LIKE;
    }

    static String getLeafImagesJo() {
        return LEAF_IMAGES_JO;
    }

    static String getFruitImagesJo() {
        return FRUIT_IMAGES_JO;
    }

    static String getFlowerImagesJo() {
        return FLOWER_IMAGES_JO;
    }

    static String getBarkImagesJo() {
        return BARK_IMAGES_JO;
    }

    //Database Name
    private static final String DATABASE_NAME = "aTreeDetails";
    private static final int DATABASE_VERSION = 1;
    //Table Name
    private static final String TREE_TABLE = "tree";
    //Tree Table Columns Name
    private static final String TREE_ID = "tree_id";
    private static final String TREE_NAME = "tree_name";

    public static String getTreeLocalName() {
        return TREE_LOCAL_NAME;
    }

    public static String getTreeBotanicalName() {
        return TREE_BOTANICAL_NAME;
    }

    private static final String TREE_LOCAL_NAME = "tree_local_name";
    private static final String TREE_BOTANICAL_NAME = "tree_botanical_name";
    private static final String TREE_IMAGES_JO = "treeimages_json_array";
    private static final String TREE_GRID_IMAGE = "tree_thumb_image";
    private static final String TREE_QR_CODE = "tree_qr_code";
    private static final String TREE_WHICH_YOU_LIKE = "which_u_like";
    private static final String LEAF_IMAGES_JO = "leafimages_json_array";
    private static final String FRUIT_IMAGES_JO = "fruitimages_json_array";
    private static final String FLOWER_IMAGES_JO = "flowerimages_json_array";
    private static final String BARK_IMAGES_JO = "barkimages_json_array";


}
