package com.itravelapp.damdama.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.itravelapp.damdama.R;

/**
 * Created by mohit on 1/12/16.
 */
public class TreasureListCellView {

    public View view;

    public ImageView thumbnailView;
    public TextView titleView;


    public TreasureListCellView(Context ctx){

        view = LayoutInflater.from(ctx).inflate(R.layout.view_cell_treasure_list, null);

        thumbnailView = (ImageView)view.findViewById(R.id.treausre_list_cell_thumbnail);
        titleView = (TextView)view.findViewById(R.id.treausre_list_cell_title);

    }
}
