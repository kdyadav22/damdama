package com.itravelapp.damdama.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.itravelapp.damdama.R;

public class MapListCellView {

    public View view;

    public TextView titleView;
    public TextView detailView;

    public MapListCellView(Context ctx){

        view = LayoutInflater.from(ctx).inflate(R.layout.view_cell_map_list, null);

        titleView = (TextView)view.findViewById(R.id.map_list_cell_title);
        detailView = (TextView)view.findViewById(R.id.map_list_cell_detail);

    }
}
