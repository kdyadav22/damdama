package com.itravelapp.damdama.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.itravelapp.damdama.R;

/**
 * Created by mohit on 2/16/16.
 */
public class ExploreListCellView {

    public View view;

    public TextView titleView;


    public ExploreListCellView(Context ctx){

        view = LayoutInflater.from(ctx).inflate(R.layout.view_cell_explore_list, null);

        titleView = (TextView)view.findViewById(R.id.explore_list_cell_title);

    }
}
