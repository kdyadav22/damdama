package com.itravelapp.damdama.utility;

/**
 * Created by Kapil on 23/10/17.
 */

public class Variables {
    private String baseUrl = "http://tajdamdama.itravelapps.co/GetTreeinfoByID.asmx/";
    private String getHomeTreeInfo = "GetHomeTreeinfo";
    private String getAllTree = "Get_AllTree_info_WithImgags";

    public String getImageBaseUrl() {
        return imageBaseUrl;
    }

    private String imageBaseUrl = "http://tajdamdama.itravelapps.co/";
    public String getBaseUrl() {
        return baseUrl;
    }

    public String getGetHomeTreeInfo() {
        return getHomeTreeInfo;
    }

    public String getGetAllTree() {
        return getAllTree;
    }

    public String getGetTreeinfoWithImgByTreeID() {
        return getTreeinfoWithImgByTreeID;
    }

    private String getTreeinfoWithImgByTreeID ="GetTreeinfoWithImgByTreeID";

    public String getHomeTreeID() {
        return HomeTreeID;
    }

    public void setHomeTreeID(String homeTreeID) {
        HomeTreeID = homeTreeID;
    }

    public String getHomeTreeImage() {
        return HomeTreeImage;
    }

    public void setHomeTreeImage(String homeTreeImage) {
        HomeTreeImage = homeTreeImage;
    }

    public String getHomeTreeDesc() {
        return HomeTreeDesc;
    }

    public void setHomeTreeDesc(String homeTreeDesc) {
        HomeTreeDesc = homeTreeDesc;
    }

    String HomeTreeID ;
    String HomeTreeImage ;
    String HomeTreeDesc;

    public String getTreeTagObj() {
        return treeTagObj;
    }

    String treeTagObj="tree_tag_obj";

    public String getTreeGridTagObj() {
        return treeGridTagObj;
    }

    String treeGridTagObj="tree_grid_tag_obj";

    public boolean isTreeDataDownloaded() {
        return isTreeDataDownloaded;
    }

    public void setTreeDataDownloaded(boolean treeDataDownloaded) {
        isTreeDataDownloaded = treeDataDownloaded;
    }

    boolean isTreeDataDownloaded=false;

}
