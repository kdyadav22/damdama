package com.itravelapp.damdama.utility;

import com.itravelapp.damdama.listeners.EventsListCallback;
import com.itravelapp.damdama.listeners.TreeGridClickListener;

/**
 * Created by ${Kapil} on 16/10/17.
 */

public class StaticConstants {
    public static TreeGridClickListener treeGridClickListener;
    public static EventsListCallback eventsListCallback;

}
