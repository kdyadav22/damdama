package com.itravelapp.damdama.volley;

/**
 * Created by kapilyadav on 2/7/2017.
 */

public class ConstantsParams {
    public static final int SOCKET_TIME_OUT = 30 * 1000;//30 seconds; Changeable
    public static final int IMAGE_SOCKET_TIME_OUT = 60 * 1000 * 2;//2 Minute; Changeable
    public static VolleyCallback volleyCallback;
    public static int time = 1000 * 60 * 2;//2 Minutes; Changeable
}
