package com.itravelapp.damdama.volley;

/**
 * Created by ${Kapil} on 06/10/17.
 */

public interface VolleyCallback {
    void onVolleyResponse(String response);
    void onVolleyError(String error);
}
