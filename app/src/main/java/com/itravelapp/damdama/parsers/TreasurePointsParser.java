package com.itravelapp.damdama.parsers;

import com.itravelapp.damdama.models.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Created by mohit.
 */
public class TreasurePointsParser extends DefaultHandler {

    String currentValue = null;

    public Vector<Zone> dataList = null;
    Zone zone;
    TreasurePoint point;
    boolean isEnabled = false;

    SAXParserFactory spf;
    SAXParser sp;

    public TreasurePointsParser() {

        spf = SAXParserFactory.newInstance();
        try {
            sp = spf.newSAXParser();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void parseXmlFile(String reqXML) {

        try {
            // get a new instance of parser
            ByteArrayInputStream bais = new ByteArrayInputStream(
                    reqXML.getBytes());
            sp.parse(bais, this);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void parseLocalXmlFile(InputStream is) {

        try {
            sp.parse(is, this);
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void parseLocalXmlFile(String path) {

        try {
            sp.parse(path, this);
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * This gets called when the xml document is first opened
     *
     * @throws SAXException
     */
    @Override
    public void startDocument() throws SAXException {
        dataList = new Vector<Zone>();
    }

    /**
     * Called when it's finished handling the document
     *
     * @throws SAXException
     */
    @Override
    public void endDocument() throws SAXException {

    }

    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        currentValue = new String();

        if (localName.equalsIgnoreCase("zone")) {
            zone = new Zone();
            zone.identifier = attributes.getValue("id");
        }
        if (dataList != null && localName.equalsIgnoreCase("point")) {
            point = new TreasurePoint();
            point.identifier = attributes.getValue("id");
            point.name = attributes.getValue("name");
        }
        if (dataList != null && point != null && localName.equalsIgnoreCase("coords")) {
            if(((String)attributes.getValue("x")).length() > 0)
                point.coordX = Integer.parseInt(attributes.getValue("x"));
            if(((String)attributes.getValue("y")).length() > 0)
                point.coordY = Integer.parseInt(attributes.getValue("y"));
        }
        if (dataList != null && point != null && localName.equalsIgnoreCase("image")) {
            point.addImage(attributes.getValue("src"));
        }

    }

    /**
     * Called when tag closing ( ex:- <name>AndroidPeople</name> -- </name> )
     */

    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        String foundCharStr = currentValue.trim();

        if (localName.equalsIgnoreCase("desc")) {
            point.description = foundCharStr;
        }else if (localName.equalsIgnoreCase("option")) {
            point.addOption(foundCharStr);
        }else if (localName.equalsIgnoreCase("code")) {
            point.code = foundCharStr;
        }else if (localName.equalsIgnoreCase("point")) {
            zone.addTreasurePoint(point);
        }else if (localName.equalsIgnoreCase("zone")) {
            dataList.addElement(zone);
        }

    }

    /**
     * Called to get tag characters ( ex:- <name>AndroidPeople</name> -- to get
     * AndroidPeople Character )
     */

    public void characters(char[] ch, int start, int length)
            throws SAXException {

        currentValue = currentValue.concat(new String(ch, start, length));

    }

}
