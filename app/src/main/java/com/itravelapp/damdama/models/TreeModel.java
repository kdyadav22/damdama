package com.itravelapp.damdama.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Kapil on 24/10/17.
 */

public class TreeModel implements Serializable{
    private ArrayList<TreeListModel> arrayList;

    public ArrayList<TreeListModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<TreeListModel> arrayList) {
        this.arrayList = arrayList;
    }
}
