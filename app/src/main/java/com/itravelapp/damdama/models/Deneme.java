package com.itravelapp.damdama.models;

/**
 * Created by Kapil on 24/10/17.
 */

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial") //With this annotation we are going to hide compiler warnings
public class Deneme implements Serializable {

    public Deneme(double id, String name, ArrayList<TreeListModel> arrayList) {
        this.id = id;
        this.name = name;
        this.arrayList = arrayList;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private double id;
    private String name;
    ArrayList<TreeListModel> arrayList;
}
