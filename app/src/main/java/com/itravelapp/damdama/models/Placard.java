package com.itravelapp.damdama.models;

import java.util.Vector;

/**
 * Created by mohit.
 */
public class Placard {

    public String identifier;
    public String name;
    public int coordX;
    public int coordY;

    private Vector<ExplorePoint> explorePoints;

    public Placard(){

        explorePoints = new Vector<ExplorePoint>();
    }

    public void addExplorePoint(ExplorePoint point){
        explorePoints.addElement(point);
    }

    public Vector<ExplorePoint> getAllExplorePoints(){

        return explorePoints;
    }

    public void printObject(){

        System.out.println("\n********Placard********\n");
    }

}
