package com.itravelapp.damdama.models;


import java.io.Serializable;

/**
 * Created by ${Kapil} on 16/10/17.
 */

public class TreeListModel implements Serializable{


    public String getTreeId() {
        return treeId;
    }

    public void setTreeId(String treeId) {
        this.treeId = treeId;
    }

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }

    private String treeId;
    private String treeName;
    String treeGridImageUrl;

    public String getTreeGridImageUrl() {
        return treeGridImageUrl;
    }

    public void setTreeGridImageUrl(String treeGridImageUrl) {
        this.treeGridImageUrl = treeGridImageUrl;
    }

    public String getTreeQRCode() {
        return treeQRCode;
    }

    public void setTreeQRCode(String treeQRCode) {
        this.treeQRCode = treeQRCode;
    }

    public String getTreesArray() {
        return treesArray;
    }

    public void setTreesArray(String treesArray) {
        this.treesArray = treesArray;
    }

    public String getLeafArray() {
        return leafArray;
    }

    public void setLeafArray(String leafArray) {
        this.leafArray = leafArray;
    }

    public String getFruitArray() {
        return fruitArray;
    }

    public void setFruitArray(String fruitArray) {
        this.fruitArray = fruitArray;
    }

    public String getFlowerArray() {
        return flowerArray;
    }

    public void setFlowerArray(String flowerArray) {
        this.flowerArray = flowerArray;
    }

    public String getBarkArray() {
        return barkArray;
    }

    public void setBarkArray(String barkArray) {
        this.barkArray = barkArray;
    }

    public String getWhichYouLike() {
        return whichYouLike;
    }

    public void setWhichYouLike(String whichYouLike) {
        this.whichYouLike = whichYouLike;
    }

    String treeQRCode;
    String treesArray, leafArray, fruitArray, flowerArray, barkArray;

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    String localName;

    public String getBotanicalName() {
        return botanicalName;
    }

    public void setBotanicalName(String botanicalName) {
        this.botanicalName = botanicalName;
    }

    String botanicalName;
    String whichYouLike;

}
