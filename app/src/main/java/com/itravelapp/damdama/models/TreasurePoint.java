package com.itravelapp.damdama.models;

import java.util.Vector;

/**
 * Created by mohit on 1/4/16.
 */
public class TreasurePoint {

    public String identifier;
    public String name;
    public String description;
    public String code;

    public int coordX;
    public int coordY;

    private Vector<String> images;
    private Vector<String> options;

    public TreasurePoint(){

        images = new Vector<String>();
        options = new Vector<String>();
    }

    public void addOption(String option){
        options.addElement(option);
    }

    public Vector<String> getAllOptions(){

        return options;
    }

    public void addImage(String imageName){
        images.addElement(imageName);
    }

    public Vector<String> getAllImages(){

        return images;
    }

    public void printObject(){

        System.out.println("\n********Treasure Point********\n" + identifier + " - " + name);
    }

}
