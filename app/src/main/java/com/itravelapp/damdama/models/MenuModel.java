package com.itravelapp.damdama.models;

/**
 * Created by Kapil on 02/11/17.
 */

public class MenuModel {
    public int getMenuImages() {
        return menuImages;
    }

    public void setMenuImages(int menuImages) {
        this.menuImages = menuImages;
    }

    public int getMenuAction() {
        return menuAction;
    }

    public void setMenuAction(int menuAction) {
        this.menuAction = menuAction;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    int menuImages, menuAction;
    String menuName;
}
