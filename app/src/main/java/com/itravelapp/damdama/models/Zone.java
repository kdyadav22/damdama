package com.itravelapp.damdama.models;

import java.util.Vector;

/**
 * Created by mohit.
 */
public class Zone {

    public String identifier;
    public String name;

    private Vector<TreasurePoint> treasurePoints;

    public Zone(){

        treasurePoints = new Vector<TreasurePoint>();
    }

    public void addTreasurePoint(TreasurePoint point){
        treasurePoints.addElement(point);
    }

    public Vector<TreasurePoint> getAllTreasurePoints(){

        return treasurePoints;
    }

    public void printObject(){

        System.out.println("\n********Zone********\n");
    }

}
