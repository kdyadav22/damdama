package com.itravelapp.damdama.sampledata;

import com.google.android.gms.maps.model.LatLng;
import com.itravelapp.damdama.models.EventsModel;
import com.itravelapp.damdama.models.TreeListModel;

import java.util.ArrayList;

/**
 * Created by kapilyadav on 4/7/2017.
 */

public class SampleData {
    /*public static ArrayList<TreeListModel> addTreeSample(){
        ArrayList<TreeListModel> arrayList = new ArrayList<TreeListModel>();

        TreeListModel treeListModel  = new TreeListModel();
        treeListModel.setTreeId("TR1");
        treeListModel.setTreeName("Kachnar");
        treeListModel.setTreeDescription("Tree Description");
        treeListModel.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel);

        TreeListModel treeListModel1  = new TreeListModel();
        treeListModel1.setTreeId("TR2");
        treeListModel1.setTreeName("Amaltas");
        treeListModel1.setTreeDescription("Tree Description");
        treeListModel1.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel1);

        TreeListModel treeListModel2  = new TreeListModel();
        treeListModel2.setTreeId("TR3");
        treeListModel2.setTreeName("Amaltas");
        treeListModel2.setTreeDescription("Tree Description");
        treeListModel2.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel2);

        TreeListModel treeListModel3  = new TreeListModel();
        treeListModel3.setTreeId("TR4");
        treeListModel3.setTreeName("Kachnar");
        treeListModel3.setTreeDescription("Tree Description");
        treeListModel3.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel3);

        TreeListModel treeListModel4  = new TreeListModel();
        treeListModel4.setTreeId("TR5");
        treeListModel4.setTreeName("Amaltas");
        treeListModel4.setTreeDescription("Tree Description");
        treeListModel4.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel4);

        TreeListModel treeListModel5  = new TreeListModel();
        treeListModel5.setTreeId("TR6");
        treeListModel5.setTreeName("Amaltas");
        treeListModel5.setTreeDescription("Tree Description");
        treeListModel5.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel5);

        TreeListModel treeListModel6  = new TreeListModel();
        treeListModel6.setTreeId("TR7");
        treeListModel6.setTreeName("Kachnar");
        treeListModel6.setTreeDescription("Tree Description");
        treeListModel6.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel6);

        TreeListModel treeListModel7  = new TreeListModel();
        treeListModel7.setTreeId("TR8");
        treeListModel7.setTreeName("Amaltas");
        treeListModel7.setTreeDescription("Tree Description");
        treeListModel7.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel7);

        TreeListModel treeListModel8  = new TreeListModel();
        treeListModel8.setTreeId("TR9");
        treeListModel8.setTreeName("Amaltas");
        treeListModel8.setTreeDescription("Tree Description");
        treeListModel8.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel8);

        TreeListModel treeListModel9  = new TreeListModel();
        treeListModel9.setTreeId("TR10");
        treeListModel9.setTreeName("Kachnar");
        treeListModel9.setTreeDescription("Tree Description");
        treeListModel9.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel9);

        TreeListModel treeListModel10  = new TreeListModel();
        treeListModel10.setTreeId("TR11");
        treeListModel10.setTreeName("Amaltas");
        treeListModel10.setTreeDescription("Tree Description");
        treeListModel10.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel10);

        TreeListModel treeListModel11  = new TreeListModel();
        treeListModel11.setTreeId("TR12");
        treeListModel11.setTreeName("Amaltas");
        treeListModel11.setTreeDescription("Tree Description");
        treeListModel11.setTreeImageUrl("kachnar.jpg");
        arrayList.add(treeListModel11);

        return arrayList;
    }*/

    public static ArrayList<EventsModel> addEventSample(){
        ArrayList<EventsModel> arrayList = new ArrayList<>();

        EventsModel model1 = new EventsModel();
        model1.setEventId("1");
        model1.setEventName("Food festival");
        model1.setEventImageUrl("");
        model1.setEventStartDate("22 Oct 2017");
        model1.setEventEndDate("15 Nov 2017");
        arrayList.add(model1);

        EventsModel model2 = new EventsModel();
        model2.setEventId("2");
        model2.setEventName("Innogration");
        model2.setEventImageUrl("");
        model2.setEventStartDate("25 Oct 2017");
        model2.setEventEndDate("05 Nov 2017");
        arrayList.add(model2);

        EventsModel model3 = new EventsModel();
        model3.setEventId("3");
        model3.setEventName("Food festival");
        model3.setEventImageUrl("");
        model3.setEventStartDate("22 Nov 2017");
        model3.setEventEndDate("05 Dec 2017");
        arrayList.add(model3);

        EventsModel mode4 = new EventsModel();
        mode4.setEventId("4");
        mode4.setEventName("Food festival");
        mode4.setEventImageUrl("");
        mode4.setEventStartDate("02 Oct 2017");
        mode4.setEventEndDate("10 Oct 2017");
        arrayList.add(mode4);

        EventsModel model5 = new EventsModel();
        model5.setEventId("5");
        model5.setEventName("Food festival");
        model5.setEventImageUrl("");
        model5.setEventStartDate("22 Oct 2017");
        model5.setEventEndDate("25 Oct 2017");
        arrayList.add(model5);

        EventsModel model6 = new EventsModel();
        model6.setEventId("6");
        model6.setEventName("Food festival");
        model6.setEventImageUrl("");
        model6.setEventStartDate("22 Oct 2017");
        model6.setEventEndDate("30 Oct 2017");
        arrayList.add(model6);


        return arrayList;
    }
}
