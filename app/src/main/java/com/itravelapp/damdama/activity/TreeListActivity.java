package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.adapters.TreeItemAdapter;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.listeners.TreeGridClickListener;
import com.itravelapp.damdama.models.TreeListModel;
import com.itravelapp.damdama.utility.StaticConstants;
import com.itravelapp.damdama.utility.Utility;

import java.util.ArrayList;

public class TreeListActivity extends AppCompatActivity implements View.OnClickListener, TreeGridClickListener {
    protected EditText etSearchTree;
    RecyclerView recyclerView;
    MVCDBController mvcdbController;
    ArrayList<TreeListModel> arrayList = new ArrayList<>();
    ArrayList<TreeListModel> tempArrayList = new ArrayList<>();
    Fragment fragment;
    FragmentManager fragmentManager;
    public int SCANNER_REQUEST_CODE = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.setContentView(R.layout.activity_tree_list);
        initView();
        mvcdbController = new MVCDBController(getApplicationContext());
        fragmentManager = getSupportFragmentManager();
        StaticConstants.treeGridClickListener = this;
        findViewById(R.id.right_bar_button).setOnClickListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Trees");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException npe) {
            npe.getMessage();
        }

        try {
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
            //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        recyclerView = (RecyclerView) findViewById(R.id.rv_treeRecyclerView);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //arrayList = mvcdbController.getAllTreeList();

        etSearchTree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Toast.makeText(getActivity(), "beforeTextChanged", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Toast.makeText(getActivity(), "onTextChanged", Toast.LENGTH_SHORT).show();
                if (charSequence.length() > 0) {
                    /*Search should be from store list*/
                    try {
                        //mClearTextButton.setVisibility(View.VISIBLE);
                        ArrayList<TreeListModel> list = filterData(arrayList, charSequence.toString().trim());
                        if (list.size() > 0) {
                            arrayList.clear();
                            arrayList = list;
                            recyclerView.setAdapter(new TreeItemAdapter(TreeListActivity.this, arrayList));
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    arrayList = mvcdbController.getAllTreeList();
                    recyclerView.setAdapter(new TreeItemAdapter(TreeListActivity.this, arrayList));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Toast.makeText(getActivity(), "afterTextChanged", Toast.LENGTH_SHORT).show();
            }
        });

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.hideSoftInput(TreeListActivity.this);
            }
        });
        /*fragment = new TreeListFragment();
        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();*/

        try {
            arrayList = mvcdbController.getAllTreeList();
            if (arrayList.size() > 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.setAdapter(new TreeItemAdapter(TreeListActivity.this, arrayList));
                    }
                });

            }
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.right_bar_button) {
            try{
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "SCAN_MODE");
                startActivityForResult(intent, SCANNER_REQUEST_CODE);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        if (requestCode == SCANNER_REQUEST_CODE) {
            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                try{
                    String contents = intent.getStringExtra("SCAN_RESULT");
                    Log.d("Scan content", "Scan content: " + contents);
                    String treeID = mvcdbController.getTreeId(contents);

                    try {
                        Intent mintent = new Intent(TreeListActivity.this, TreeDetailActivity.class);
                        mintent.putExtra("TreeName", mvcdbController.getTreeName(contents));
                        mintent.putExtra("TreeID", mvcdbController.getTreeId(contents));
                        startActivity(mintent);
                        etSearchTree.setText("");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(TreeListActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    public void onGridClick(int pos) {
        try {
            /*Intent intent = new Intent(TreeListActivity.this, TreeDetailActivity.class);
            intent.putExtra("TreeName", arrayList.get(pos).getTreeName());
            intent.putExtra("TreeID", arrayList.get(pos).getTreeId());
            startActivity(intent);*/
            etSearchTree.setText("");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private ArrayList<TreeListModel> filterData(ArrayList<TreeListModel> list, String string) {
        final ArrayList<TreeListModel> arrayList = new ArrayList<>();
        try {
            for (int i = 0; i < list.size(); i++) {
                TreeListModel model = new TreeListModel();
                final String sName = list.get(i).getTreeName().toLowerCase();
                if (sName.startsWith(string.toLowerCase())) {
                    model.setTreeId(list.get(i).getTreeId());
                    model.setTreeName(list.get(i).getTreeName());
                    model.setLocalName(list.get(i).getLocalName());
                    model.setBotanicalName(list.get(i).getBotanicalName());
                    model.setTreeGridImageUrl(list.get(i).getTreeGridImageUrl());
                    model.setTreesArray(list.get(i).getTreesArray());
                    model.setLeafArray(list.get(i).getLeafArray());
                    model.setFruitArray(list.get(i).getFruitArray());
                    model.setFlowerArray(list.get(i).getFlowerArray());
                    model.setBarkArray(list.get(i).getBarkArray());
                    model.setWhichYouLike(list.get(i).getWhichYouLike());
                    model.setTreeQRCode(list.get(i).getTreeQRCode());
                    arrayList.add(model);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return arrayList;
    }

    private void initView() {
        etSearchTree = (EditText) findViewById(R.id.et_search_tree);
    }
}
