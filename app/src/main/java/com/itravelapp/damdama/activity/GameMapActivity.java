package com.itravelapp.damdama.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.TreasurePoint;

import java.util.Vector;

public class GameMapActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout baseView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Map");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        baseView = (RelativeLayout)findViewById(R.id.game_map_baseview);
        baseView.post(new Runnable() {
            @Override
            public void run() {
                calculateCoordinatesForLabels();
            }
        });

    }

    public void onClick(View v){

        /*
        if(v.getId() == R.id.game_map_next){

            Intent intent = new Intent(GameMapActivity.this, GameCertificateActivity.class);
            startActivity(intent);
            finish();
        }
        */
    }

    private void calculateCoordinatesForLabels(){

        RelativeLayout baseView = (RelativeLayout)findViewById(R.id.game_map_baseview);
        final int oldWidth = 1024, oldHeight = 448;
        int availableWidth = baseView.getMeasuredWidth(), availableHeight = baseView.getMeasuredHeight();
        final int newWidth, newHeight;
        if(((float)oldWidth/(float)oldHeight) > ((float)availableWidth/(float)availableHeight)){
            newWidth = availableWidth;
            newHeight = (availableWidth * oldHeight) / oldWidth;
        }else{
            newHeight = availableHeight;
            newWidth = (availableHeight * oldWidth) / oldHeight;
        }

        runOnUiThread(new Runnable() {

            public void run() {
                resizeBaseViewAndAddLabels(newWidth, newHeight, ((float) newWidth / (float) oldWidth));
            }
        });

    }

    private void resizeBaseViewAndAddLabels(int newWidth, int newHeight, float ratio){

        int labelWidth = (int)(ratio * 50);
        int labelHeight = (int)(ratio * 50);

        Vector<TreasurePoint> treasurePoints = DataManager.getInstance().treasurePointsList;


        for(int i = 0 ; i < treasurePoints.size() ; i++){

            TreasurePoint treasurePoint = treasurePoints.elementAt(i);
            Integer status = DataManager.getInstance().completedList.elementAt(i);
            if(status.intValue() == 0)
                continue;

            int oldX = treasurePoint.coordX, oldY = treasurePoint.coordY;
            int newX = (int)(ratio * oldX);
            int newY = (int)(ratio * oldY);

            int drawX, drawY;
            drawX = newX - (labelWidth / 2) ;
            drawY = newY - labelHeight ;

            //RelativeLayout.LayoutParams labelParams = new RelativeLayout.LayoutParams(labelWidth,labelHeight);//RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            //labelParams.addRule(RelativeLayout.X, drawX);
            //labelParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            //baseView.setLayoutParams(labelParams);

            ImageView labelView = new ImageView(this);
            labelView.setX(drawX);
            labelView.setY(drawY);
            labelView.setBackgroundResource(R.drawable.treasure_found);
            labelView.setLayoutParams(new RelativeLayout.LayoutParams(labelWidth, labelHeight));
            labelView.setScaleType(ImageView.ScaleType.FIT_XY);
            baseView.addView(labelView, labelWidth, labelHeight);
            labelView.setTag("" + i);
            labelView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }

    }

}
