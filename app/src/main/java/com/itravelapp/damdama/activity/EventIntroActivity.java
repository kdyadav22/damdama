package com.itravelapp.damdama.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.adapters.EventsAdapter;
import com.itravelapp.damdama.listeners.EventsListCallback;
import com.itravelapp.damdama.models.EventsModel;
import com.itravelapp.damdama.sampledata.SampleData;
import com.itravelapp.damdama.utility.StaticConstants;

import java.util.ArrayList;

public class EventIntroActivity extends AppCompatActivity implements View.OnClickListener, EventsListCallback {
    RecyclerView recyclerView;
    CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_intro);
        StaticConstants.eventsListCallback = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Events");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //setSupportActionBar(toolbar);
        findViewById(R.id.right_bar_button).setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.rv_eventRecyclerView);
        calendarView = (CalendarView) findViewById(R.id.simpleCalendarView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setEventAdapter();

        // perform setOnDateChangeListener event on CalendarView
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                // display the selected date by using a toast
                Toast.makeText(getApplicationContext(), dayOfMonth + "/" + month + "/" + year, Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.right_bar_button) {
            findViewById(R.id.parentlayout).refreshDrawableState();
            calendarView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(int pos) {

    }

    private void setEventAdapter() {
        recyclerView.setAdapter(new EventsAdapter(EventIntroActivity.this, SampleData.addEventSample()));
    }

    private ArrayList<EventsModel> filteredEventList(String date) {

        return null;
    }

}
