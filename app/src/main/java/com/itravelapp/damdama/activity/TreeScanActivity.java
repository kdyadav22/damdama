package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.itravelapp.damdama.R;

public class TreeScanActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView scanButton;
    public int SCANNER_REQUEST_CODE = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_scan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Scan QR code");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try{
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        scanButton = (ImageView)findViewById(R.id.tree_scan_button);
        scanButton.setOnClickListener(this);
    }

    public void onClick(View v){
        if(v.getId() == R.id.tree_scan_button){
            try{
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "SCAN_MODE");
            startActivityForResult(intent, SCANNER_REQUEST_CODE);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        if (requestCode == SCANNER_REQUEST_CODE) {
            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                try{
                // Handle successful scan
                String contents = intent.getStringExtra("SCAN_RESULT");
                Log.d("Scan content", "Scan content: "+contents);
                Intent scanIntent1 = new Intent(getApplicationContext(), TreeDetailActivity.class);
                scanIntent1.putExtra("QR_Content", contents);
                scanIntent1.putExtra("IsFromScan", true);
                startActivity(scanIntent1);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

}
