package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.ExplorePoint;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class ExploreActivity extends AppCompatActivity implements OnClickListener {

    Activity currentActivity = null;

    ListView listView = null;
    ListCellAdapter listCellAdapter = null;

    Vector<ExplorePoint> dataList = null;
    int cellWidth, cellHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        currentActivity = this;
        dataList = DataManager.getInstance().selectedPlacard.getAllExplorePoints();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(DataManager.getInstance().selectedPlacard.name);
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        calculateCellHeight();
        listView = (ListView) findViewById(R.id.explore_points_list);
        listCellAdapter = new ListCellAdapter();
        listView.setOnItemClickListener(didSelectedListCell);
        listView.post(new Runnable() {
            @Override
            public void run() {

            }
        });

        setAdapter();

    }

    public void calculateCellHeight() {
        cellWidth = DataManager.getInstance().screenWidth;
        cellHeight = (cellWidth * 240) / 320;
    }

    public void onClick(View arg0) {


    }

    public void setAdapter() {

        if (listView.getAdapter() == null && listCellAdapter != null) {
            listView.setAdapter(listCellAdapter);
        }
        listCellAdapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener didSelectedListCell = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int index,
                                long arg3) {

            DataManager.getInstance().selectedExplorePoint = DataManager.getInstance().selectedPlacard.getAllExplorePoints().elementAt(index);
            Intent intent = new Intent(ExploreActivity.this, RoomDetailActivity.class);
            startActivity(intent);

        }

    };

    public void populateView(View view, int position) {

        ViewHolder holder = (ViewHolder) view.getTag();

        ExplorePoint point = dataList.elementAt(position);
        holder.titleView.setText(point.name);

        try {

            holder.thumbnailView.setMinimumHeight(cellHeight);
            holder.thumbnailView.setMaxHeight(cellHeight);

            InputStream is = getAssets().open(point.getAllImages().elementAt(0));
            //InputStream is = openFileInput(Utility.getUniqueFileNameFromURL(selectedAudioTour.topViewImageURL));
            Bitmap image = BitmapFactory.decodeStream(is);
            holder.thumbnailView.setImageBitmap(image);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    class ListCellAdapter extends BaseAdapter {

        public int getCount() {

            if (dataList != null)
                return dataList.size();

            return 0;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflateView();
            }
            populateView(convertView, position);
            return convertView;
        }

        private View inflateView() {

            ExploreListCellView cellView = new ExploreListCellView(currentActivity);

            ViewHolder vh = new ViewHolder();
            vh.thumbnailView = cellView.thumbnailView;
            vh.titleView = cellView.titleView;

            cellView.view.setTag(vh);

            return cellView.view;

        }

    }

    class ExploreListCellView {

        public View view;

        public ImageView thumbnailView;
        public TextView titleView;

        public ExploreListCellView(Context ctx) {

            view = LayoutInflater.from(ctx).inflate(R.layout.view_cell_explore_points, null);

            thumbnailView = (ImageView) view.findViewById(R.id.explore_points_cell_image);
            titleView = (TextView) view.findViewById(R.id.explore_points_cell_title);

        }
    }

    private static class ViewHolder {

        ImageView thumbnailView;
        TextView titleView;

    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(ExploreActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}