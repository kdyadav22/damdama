package com.itravelapp.damdama.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

public class GameCertificateActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_certificate);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Certificate");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        //upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        //getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        int imageID = R.drawable.certificate_no;
        int treasuresFound = DataManager.getInstance().treasuresFound;
        if(treasuresFound == (DataManager.getInstance().treasurePointsList.size()))
            imageID = R.drawable.certificate_gold;
        else if(treasuresFound == (DataManager.getInstance().treasurePointsList.size() - 1))
            imageID = R.drawable.certificate_silver;
        else if(treasuresFound == (DataManager.getInstance().treasurePointsList.size() - 2))
            imageID = R.drawable.certificate_bronze;

        ((ImageView)findViewById(R.id.game_certificate_claim)).setImageResource(imageID);

        Vector<String> qrCodeInfo = DataManager.getInstance().qrCodeInfo;
        String guestName = qrCodeInfo.elementAt(0) + " " + qrCodeInfo.elementAt(1);
        String guestRooom = "Room no: " + qrCodeInfo.elementAt(2);
        Date todayDate = new Date();

//                DateFormat.getDateInstance(),
//                DateFormat.getDateTimeInstance(),
//                DateFormat.getTimeInstance(),
//

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        String guestGameDate = "Date of completion: " + sdf.format(todayDate);

        //String guestGameDate = "Date of completion: " + DateFormat.getDateTimeInstance().format(todayDate);;
        //String guestGameDate = "Date of completion: " + todayDate.toString();

        ((TextView) findViewById(R.id.cert_name_label)).setText(guestName);
        ((TextView)findViewById(R.id.cert_room_label)).setText(guestRooom);
        ((TextView)findViewById(R.id.cert_date_label)).setText(guestGameDate);

        ((TextView)findViewById(R.id.game_certificate_done_button)).setOnClickListener(this);

        TextView timeLabel = (TextView)findViewById(R.id.cert_time_label);

        Date currentDateTime = new Date();
        long duration = (currentDateTime.getTime() - DataManager.getInstance().gameStartTime.getTime());
        if(duration >= DataManager.getInstance().maxGameTime){
            timeLabel.setText(formatDurationToString(DataManager.getInstance().maxGameTime));
        }else {
            timeLabel.setText(formatDurationToString(duration));
        }

    }

    public String formatDurationToString(long durationInMillis){

        int durationInSecs = (int)(durationInMillis / 1000);
        int hours = durationInSecs / 3600;
        int minutes = durationInSecs / 60;
        int secs = durationInSecs % 60;

        return String.format("%02d:%02d:%02d",hours,minutes,secs);
    }

    public void onClick(View v){

        if(v.getId() == R.id.game_certificate_done_button){

            finish();
            System.gc();
            return;
        }

    }

}
