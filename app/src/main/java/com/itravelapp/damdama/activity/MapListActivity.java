package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.views.MapListCellView;

public class MapListActivity extends AppCompatActivity {

    Activity currentActivity  = null;
    ListView listView = null;
    ListCellAdapter listCellAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Directions");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);

        currentActivity = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        listView = (ListView)findViewById(R.id.map_list);
        listView.post(new Runnable() {
            @Override
            public void run() {
                DataManager.getInstance().screenWidth = listView.getWidth();
            }
        });

        listCellAdapter = new ListCellAdapter();
        listView.setOnItemClickListener(didSelectedListCell);
        setAdapter();


    }

    public void setAdapter() {

        if (listView.getAdapter() == null && listCellAdapter != null) {
            listView.setAdapter(listCellAdapter);
        }
        listCellAdapter.notifyDataSetChanged();
    }


    private AdapterView.OnItemClickListener didSelectedListCell = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int index,
                                long arg3) {

            Intent intent = new Intent(MapListActivity.this, MapActivity.class);
            intent.putExtra("selected_item",index);
            startActivity(intent);

        }

    };

    class ListCellAdapter extends BaseAdapter {

        public int getCount() {

            return  2;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflateView();
            }
            populateView(convertView, position);
            return convertView;
        }

        private View inflateView() {

            MapListCellView cellView = new MapListCellView(currentActivity);

            ViewHolder vh = new ViewHolder();
            vh.titleView = cellView.titleView;
            vh.detailView = cellView.detailView;

            cellView.view.setTag(vh);

            return cellView.view;

        }

    }

    public void populateView(View view, int position) {

        ViewHolder holder = (ViewHolder) view.getTag();

        if(position == 0){
            holder.titleView.setText("From Delhi to The Gateway Resort, Damdama");
            holder.detailView.setText("Tap for a detailed map from Rajiv Chowk, Gurgaon to The Gateway Resort, Damdama");
        }else if(position == 1){
            holder.titleView.setText("From Faridabad to The Gateway Resort, Damdama");
            holder.detailView.setText("Tap for a detailed map from Faridabad to The Gateway Resort, Damdama");
        }
    }


    private static class ViewHolder {

        TextView titleView;
        TextView detailView;
    }
}
