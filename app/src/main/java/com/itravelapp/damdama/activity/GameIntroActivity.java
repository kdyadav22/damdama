package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;

//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;
import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;

import java.util.HashSet;
import java.util.Vector;

public class GameIntroActivity extends AppCompatActivity implements OnClickListener {

    public int SCANNER_REQUEST_CODE = 123;

    private Button scanBtn;
    private TextView formatTxt, contentTxt;

    private ImageView startButton;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
   // private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_intro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Treasure Hunt");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

//        scanBtn = (Button)findViewById(R.id.scan_button);
//        formatTxt = (TextView)findViewById(R.id.scan_format);
//        contentTxt = (TextView)findViewById(R.id.scan_content);

        String str = "Scan the QR code and let's play\n" +
                "\n" +
                "If you find all 12 treasures, it's a big yay\n" +
                "\n" +
                "Keep cracking the clues along the way\n" +
                "\n" +
                "Remember, you have an hour to make your day\n" +
                "\n" +
                "Even if you find 10 or 11, feel happy and gay\n" +
                "\n" +
                "'cause at all those points, we've much to give away ";

        ((TextView) findViewById(R.id.game_intro_text)).setText(str);

        startButton = (ImageView) findViewById(R.id.game_intro_start_button);
        startButton.setOnClickListener(this);

//        scanBtn.setOnClickListener(this);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    public void onClick(View v) {

        /*
        if(v.getId()==R.id.scan_button){


//            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
//            intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE", "QR_CODE_MODE");
//            startActivityForResult(intent, 0);
        }
        */

        if (v.getId() == R.id.game_intro_start_button) {

//            processCode("", "&#S2958$3335282940&#S2958$2735452132&#S2958$2738242022&#S2958$2125212122202125&#S2958$2127212122202125&#S2958$111014&#S2958$111012&#S2958$");
//            return;
//            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
//            scanIntegrator.initiateScan();

            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "SCAN_MODE");
            startActivityForResult(intent, SCANNER_REQUEST_CODE);

        }
    }

    private void processCode(String scanFormat, String scanContent) {

        Vector<String> processedCode = DataManager.decodeQRCodeData(scanContent);

        if (processedCode.size() >= 7) {

            SharedPreferences myPreferences = getSharedPreferences(
                    "SCANNED_CODES", MODE_PRIVATE);
            HashSet<String> allStoredCodes = (HashSet<String>) myPreferences.getStringSet("CODES", null);

            String newCustomerID = processedCode.elementAt(5);

            System.out.println("New customer Id "+ newCustomerID);

            if (allStoredCodes != null) {
                if(allStoredCodes.contains(newCustomerID)){
                    showAlert("You have already scanned this QR code in past.");
                    return;
                }
            }

            SharedPreferences.Editor myPreferencesEditor = myPreferences.edit();
            if(allStoredCodes == null)
                allStoredCodes = new HashSet<String>();
            allStoredCodes.add(newCustomerID);
            myPreferencesEditor.putStringSet("CODES",allStoredCodes);
            myPreferencesEditor.commit();

            DataManager.getInstance().qrCodeInfo = processedCode;
            Intent intent = new Intent(GameIntroActivity.this, GameStartActivity.class);
            String clientName = "" + processedCode.elementAt(0) + " " + processedCode.elementAt(1);
            intent.putExtra("CLIENT_NAME", clientName);
            startActivity(intent);
            finish();
        } else {
            showAlert("Incorrect code scanned");
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result

        if (requestCode == SCANNER_REQUEST_CODE) {

            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                String contents = intent.getStringExtra("SCAN_RESULT");
                String formatName = intent.getStringExtra("SCAN_RESULT_FORMAT");
                byte[] rawBytes = intent.getByteArrayExtra("SCAN_RESULT_BYTES");
                int intentOrientation = intent.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
                Integer orientation = (intentOrientation == Integer.MIN_VALUE) ? null : intentOrientation;
                String errorCorrectionLevel = intent.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL");

                processCode(formatName, contents);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }

        }

    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameIntroActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        /*
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "GameIntro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.itravelapp.testprj4/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);

        */
    }

    @Override
    public void onStop() {
        super.onStop();

        /*
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "GameIntro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.itravelapp.testprj4/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
        */
    }
}
