package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.TreasurePoint;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class GameQuestionActivity extends AppCompatActivity  implements View.OnClickListener {

    public int SCANNER_REQUEST_CODE = 123;

    private ImageView scanButton, skipButton;
    private int currentQuestionIndex = -1;
    Boolean shouldStopThread = false;
    TextView timeLabel;
    Boolean fromTreasureList = false;
    TreasurePoint point = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_question);

        currentQuestionIndex = getIntent().getIntExtra("CURRENT_INDEX",-1);
        point = DataManager.getInstance().treasurePointsList.elementAt(currentQuestionIndex);

        fromTreasureList = getIntent().getBooleanExtra("FROM_TREASURELIST",false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Riddle "+(currentQuestionIndex + 1));
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);

        ((TextView)findViewById(R.id.game_question_map)).setOnClickListener(this);

        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        //upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        //getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        scanButton = (ImageView)findViewById(R.id.game_question_scan);
        scanButton.setOnClickListener(this);

        skipButton = (ImageView)findViewById(R.id.game_question_skip);
        skipButton.setOnClickListener(this);

        timeLabel = (TextView)findViewById(R.id.game_question_time_label);

        String optionStr = "";
        for (int i = 0 ; i < point.getAllOptions().size(); i++){
            if(i > 0)
                optionStr += "\n";
            optionStr += point.getAllOptions().elementAt(i);
        }

        ((TextView)findViewById(R.id.game_question_description)).setText(optionStr);

        if(point.getAllImages().size() > 0) {

            try {
                InputStream is = getAssets().open(point.getAllImages().elementAt(0));
                //InputStream is = openFileInput(Utility.getUniqueFileNameFromURL(selectedAudioTour.topViewImageURL));
                Bitmap image = BitmapFactory.decodeStream(is);
                ImageView questionImageView = (ImageView)findViewById(R.id.game_question_clue_image);
                questionImageView.setImageBitmap(image);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        shouldStopThread = false;
        new Thread(new Runnable() {
            public void run() {
                System.out.println("check");
                while (shouldStopThread == false) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    updateTimeLabel();
                }
            }
        }).start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        shouldStopThread = true;
    }

    private void updateTimeLabel(){

        runOnUiThread(new Runnable() {

            public void run() {
                Date currentDateTime = new Date();
                long duration = (currentDateTime.getTime() - DataManager.getInstance().gameStartTime.getTime());
                if(duration >= DataManager.getInstance().maxGameTime){

                    shouldStopThread = true;
                    timeLabel.setText("Time over");
                    showAlert("Time is Over!");


                    if(fromTreasureList){
                        finish();
                        System.gc();
                        return;
                    }else{

                        Intent intent = new Intent(GameQuestionActivity.this, GameTreasureListActivity.class);
                        startActivity(intent);
                        finish();
                        System.gc();
                    }

                }else {
                    timeLabel.setText(formatDurationToString(duration));
                }
            }
        });

    }

    public String formatDurationToString(long durationInMillis){

        int durationInSecs = (int)(durationInMillis / 1000);
        int hours = durationInSecs / 3600;
        int minutes = durationInSecs / 60;
        int secs = durationInSecs % 60;

        return String.format("%02d:%02d:%02d",hours,minutes,secs);
    }

    public void onClick(View v){

        if(v.getId() == R.id.game_question_scan){

            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "SCAN_MODE");
            startActivityForResult(intent, SCANNER_REQUEST_CODE);

        }

        if(v.getId() == R.id.game_question_skip){

            shouldStopThread = true;
            showNextScreen();

        }

        if(v.getId() == R.id.game_question_map){

            Intent intent = new Intent(GameQuestionActivity.this, GameMapActivity.class);
            startActivity(intent);

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == SCANNER_REQUEST_CODE) {

            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                String contents = intent.getStringExtra("SCAN_RESULT");
                String formatName = intent.getStringExtra("SCAN_RESULT_FORMAT");
                byte[] rawBytes = intent.getByteArrayExtra("SCAN_RESULT_BYTES");
                int intentOrientation = intent.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
                Integer orientation = (intentOrientation == Integer.MIN_VALUE) ? null : intentOrientation;
                String errorCorrectionLevel = intent.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL");

                processCode(formatName, contents);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    }

    private void processCode(String scanFormat, String scanContent){

        String code = point.code;

        if(scanContent != null && scanContent.length() > 0 && scanContent.equalsIgnoreCase(code)){

            DataManager.getInstance().treasuresFound++;
            DataManager.getInstance().completedList.setElementAt(new Integer(1), currentQuestionIndex);

            AlertDialog.Builder builder = new AlertDialog.Builder(GameQuestionActivity.this);

            String msg = "You've hit bull's eye\nTime to go to the next high.";

            if(DataManager.getInstance().treasuresFound == (DataManager.getInstance().treasurePointsList.size() - 2))
                msg = "Bring out the bugles, blare the horns\nTime to celebrate, you just got Bronze";
            else if(DataManager.getInstance().treasuresFound == (DataManager.getInstance().treasurePointsList.size() - 1))
                msg = "Make way for the real achiever\nYo, it's true, you just got silver";
            else if(DataManager.getInstance().treasuresFound == (DataManager.getInstance().treasurePointsList.size()))
                msg = "Yippie and yoodle and lo and behold\nWould you believe it, you just got Gold";

            builder.setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    showNextScreen();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

        }else{
            showAlert("Wrong point scanned!! Try again.");
        }

    }

    public void showNextScreen(){

        if(fromTreasureList){
            finish();
            System.gc();
            return;
        }

        if(currentQuestionIndex < (DataManager.getInstance().treasurePointsList.size() - 1)){

            System.gc();

            Intent intent = new Intent(GameQuestionActivity.this, GameQuestionActivity.class);
            intent.putExtra("CURRENT_INDEX",currentQuestionIndex + 1);
            intent.putExtra("FROM_TREASURELIST",false);
            startActivity(intent);
            finish();

        }else{

            Intent intent = new Intent(GameQuestionActivity.this, GameTreasureListActivity.class);
            startActivity(intent);
            finish();

        }

    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameQuestionActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

}
