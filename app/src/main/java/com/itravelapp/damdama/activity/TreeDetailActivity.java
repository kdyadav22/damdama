package com.itravelapp.damdama.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.fragment.FlowerFragment;
import com.itravelapp.damdama.fragment.FruitFragment;
import com.itravelapp.damdama.fragment.LeafFragment;
import com.itravelapp.damdama.fragment.MoreFragment;
import com.itravelapp.damdama.fragment.TreeDetails;
import com.itravelapp.damdama.volley.AppController;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class TreeDetailActivity extends AppCompatActivity {
    ActionBar actionbar;
    TextView textview;
    RelativeLayout.LayoutParams layoutparams;
    public Toolbar toolbar;
    public BottomNavigationView navigation;
    String treeID;
    Fragment fragment;
    FragmentManager fragmentManager;
    MVCDBController mvcdbController;
    private static TreeDetailActivity mInstance;
    public int statusBarHeight = 0;
    public int actionBarHeight = 0;
    public int navigationBarHeight = 0;

    public static synchronized TreeDetailActivity getInstance() {
        return mInstance;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            try {
                switch (item.getItemId()) {
                    case R.id.navigation_treeinfo:
                        Bundle bundle = new Bundle();
                        bundle.putString("TreeID", treeID);
                        fragment = new TreeDetails();
                        fragment.setArguments(bundle);
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                        return true;
                    case R.id.navigation_leaf:
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("TreeID", treeID);
                        fragment = new LeafFragment();
                        fragment.setArguments(bundle1);
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                        return true;
                    case R.id.navigation_flower:
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("TreeID", treeID);
                        fragment = new FlowerFragment();
                        fragment.setArguments(bundle2);
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                        return true;
                    case R.id.navigation_fruit:
                        Bundle bundle3 = new Bundle();
                        bundle3.putString("TreeID", treeID);
                        fragment = new FruitFragment();
                        fragment.setArguments(bundle3);
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                        return true;
                    case R.id.navigation_more:
                        Bundle bundle4 = new Bundle();
                        bundle4.putString("TreeID", treeID);
                        fragment = new MoreFragment();
                        fragment.setArguments(bundle4);
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                        return true;
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_detail);
        mInstance = this;
        mvcdbController = new MVCDBController(getApplicationContext());
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        disableShiftMode(navigation);
        fragmentManager = getSupportFragmentManager();
        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        if (getIntent().hasExtra("TreeName"))
            mTitle.setText(getIntent().getStringExtra("TreeName"));

        /*toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);*/
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException npe) {
            npe.getMessage();
        }

        centerToolbarTitle(toolbar);

        if (getIntent().getBooleanExtra("IsFromScan", false)) {
            //AppController.getInstance().setMoreButtons(false);
            treeID = mvcdbController.getTreeId(getIntent().getStringExtra("QR_Content"));
        } else
            treeID = getIntent().getStringExtra("TreeID");

        displayToolbar();

        // status bar height
        try {
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // action bar height
        try {
            final TypedArray styledAttributes = getApplicationContext().getTheme().obtainStyledAttributes(
                    new int[]{android.R.attr.actionBarSize}
            );
            actionBarHeight = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // navigation bar height
        try {
            int resourceId1 = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId1 > 0) {
                navigationBarHeight = getResources().getDimensionPixelSize(resourceId1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            Bundle bundle = new Bundle();
            bundle.putString("TreeID", treeID);
            fragment = new TreeDetails();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void displayToolbar() {
        try {
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppController.getInstance().isMoreOptionsClicked()) {
                        AppController.getInstance().setMoreButtons(false);
                        Bundle bundle4 = new Bundle();
                        bundle4.putString("TreeID", treeID);
                        fragment = new MoreFragment();
                        fragment.setArguments(bundle4);
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                    } else {
                        onBackPressed();
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public static void centerToolbarTitle(final Toolbar toolbar) {
        final CharSequence title = toolbar.getTitle();
        final ArrayList<View> outViews = new ArrayList<>(1);
        toolbar.findViewsWithText(outViews, title, View.FIND_VIEWS_WITH_TEXT);
        if (!outViews.isEmpty()) {
            final TextView titleView = (TextView) outViews.get(0);
            titleView.setGravity(Gravity.CENTER_HORIZONTAL);
            final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) titleView.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.setMargins(0,0,60,0);
            toolbar.requestLayout();
        }
    }

}