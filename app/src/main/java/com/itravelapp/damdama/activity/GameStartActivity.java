package com.itravelapp.damdama.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.models.TreasurePoint;
import com.itravelapp.damdama.models.Zone;
import com.itravelapp.damdama.parsers.TreasurePointsParser;
import com.itravelapp.damdama.managers.*;

import java.io.InputStream;
import java.util.Date;
import java.util.Vector;

public class GameStartActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView gameStartButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_start);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Start Game");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        String clientName = getIntent().getStringExtra("CLIENT_NAME");
        if(clientName == null)
            clientName = "Hello";

        String str = clientName +" Dear," +
        "\n\nThank you for being here" +
        "\n\nHope the Treasure Hunt puts your spirits in top gear" +
        "\n\nYou may play this alone or in pairs" +
        "\n\nExciting gifts await, so have no fear" +
        "\n\nFrom the entire team at Taj Gateway Resort" +
        "\n\nWelcome and a big cheer" +
        "\n\nGreetings from" +
        "\n\n(Manager's name)," +
        "\n\nheartfelt and sincere";

        ((TextView)findViewById(R.id.game_start_text)).setText(str);

        gameStartButton = (ImageView)findViewById(R.id.game_start_button);
        gameStartButton.setOnClickListener(this);
    }

    public void onClick(View v){

        if(v.getId() == R.id.game_start_button){

            loadData();
            Intent intent = new Intent(GameStartActivity.this, GameQuestionActivity.class);
            intent.putExtra("CURRENT_INDEX",0);
            startActivity(intent);
            finish();
        }
    }

    public void loadData(){

        try{
            InputStream is = getApplicationContext().getAssets().open("zones.xml");
            TreasurePointsParser parser = new TreasurePointsParser();
            parser.parseLocalXmlFile(is);
            if(parser.dataList != null && parser.dataList.size() > 0) {
                Vector<Zone> allZones = parser.dataList;
                Vector<TreasurePoint> points = new Vector<TreasurePoint>();

                boolean isTeam = false;
                String personType = DataManager.getInstance().qrCodeInfo.elementAt(6);
                if(personType.equalsIgnoreCase("102"))
                    isTeam = true;

                for (int i = 0; i < allZones.size(); i++){

                    Zone zone = allZones.elementAt(i);
                    Vector<TreasurePoint> zonePoints = zone.getAllTreasurePoints();
                    int count = 2;
                    if(isTeam)
                        count = 3;
                    Vector<Integer> randomNumbers = DataManager.generateRandomUniqueNumberInRange(1,zonePoints.size(),3);
                    points.addElement(zonePoints.elementAt(randomNumbers.elementAt(0).intValue() - 1));
                    points.addElement(zonePoints.elementAt(randomNumbers.elementAt(1).intValue() - 1));
                    if(isTeam)
                        points.addElement(zonePoints.elementAt(randomNumbers.elementAt(2).intValue() - 1));
                }

                DataManager.getInstance().treasurePointsList = points;

                Vector<Integer> completedList = new Vector<Integer>();
                for (int i = 0; i < points.size(); i++){
                    completedList.addElement(new Integer(0));
                    points.elementAt(i).printObject();
                }
                DataManager.getInstance().completedList = completedList;

                DataManager.getInstance().treasuresFound = 0;

                DataManager.getInstance().gameStartTime = new Date();
            }

        }catch(Exception e){
            e.printStackTrace();
            System.out.println("MohitError: Error in zones.xml");
            showAlert("Error in zones.xml\n" + e.getMessage());
            //errorWhileRecievingData("Error in locations.xml\n" + e.getMessage());
        }

    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameStartActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

}
