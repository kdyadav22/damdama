package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.Placard;
import com.itravelapp.damdama.views.ExploreListCellView;

import java.util.Vector;

public class ExploreListActivity extends AppCompatActivity implements View.OnClickListener {

    Activity currentActivity = null;

    ListView listView = null;
    ListCellAdapter listCellAdapter = null;

    Vector<Placard> dataList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore_list);

        currentActivity = this;
        dataList = DataManager.getInstance().explorePlacards;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Explore");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        ((TextView) findViewById(R.id.right_bar_button)).setOnClickListener(this);

        listView = (ListView) findViewById(R.id.explore_list);
        listView.post(new Runnable() {
            @Override
            public void run() {
                DataManager.getInstance().screenWidth = listView.getWidth();
            }
        });

        listCellAdapter = new ListCellAdapter();
        listView.setOnItemClickListener(didSelectedListCell);
        setAdapter();

    }

    public void onClick(View v) {

        if (v.getId() == R.id.right_bar_button) {

            Intent intent = new Intent(ExploreListActivity.this, ExploreMapActivity.class);
            startActivity(intent);
            return;
        }

    }

    public void setAdapter() {

        if (listView.getAdapter() == null && listCellAdapter != null) {
            listView.setAdapter(listCellAdapter);
        }
        listCellAdapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener didSelectedListCell = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int index,
                                long arg3) {

            DataManager.getInstance().selectedPlacard = DataManager.getInstance().explorePlacards.elementAt(index);

            Intent intent = new Intent(ExploreListActivity.this, ExploreActivity.class);
            startActivity(intent);

        }

    };

    public void populateView(View view, int position) {

        ViewHolder holder = (ViewHolder) view.getTag();

        Placard placard = dataList.elementAt(position);

        holder.titleView.setText(placard.name);

    }

    class ListCellAdapter extends BaseAdapter {

        public int getCount() {

            if (dataList != null)
                return dataList.size();

            return 0;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflateView();
            }
            populateView(convertView, position);
            return convertView;
        }

        private View inflateView() {

            ExploreListCellView cellView = new ExploreListCellView(currentActivity);

            ViewHolder vh = new ViewHolder();
            vh.titleView = cellView.titleView;

            cellView.view.setTag(vh);

            return cellView.view;

        }

    }

    private static class ViewHolder {

        TextView titleView;

    }

}
