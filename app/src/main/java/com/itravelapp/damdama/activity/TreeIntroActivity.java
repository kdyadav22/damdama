package com.itravelapp.damdama.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.itravelapp.damdama.R;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.dialog.AlertDialogManager;
import com.itravelapp.damdama.imageloader.FileCache;
import com.itravelapp.damdama.imageloader.ImageLoader;
import com.itravelapp.damdama.models.TreeListModel;
import com.itravelapp.damdama.models.TreeModel;
import com.itravelapp.damdama.utility.CheckConnection;
import com.itravelapp.damdama.utility.Utility;
import com.itravelapp.damdama.utility.Variables;
import com.itravelapp.damdama.volley.AppController;
import com.itravelapp.damdama.volley.ConstantsParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.google.android.gms.wearable.DataMap.TAG;
import static com.itravelapp.damdama.R.drawable.abc_ic_ab_back_material;

public class TreeIntroActivity extends AppCompatActivity implements View.OnClickListener {
    ProgressBar progressBar, homeTreeProgressBar, right_bar_prb;
    TreeModel treeModel;
    TextView treeIntroTextView;
    ImageView treeIntroImageView;
    Variables variables = new Variables();
    MVCDBController mvcdbController;
    FileCache fileCache;
    ImageLoader imageLoader;

    public static String[] PERMISSIONS_CONTACT = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    public static int REQUEST_CONTACTS = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_intro);
        askForPermission();
        mvcdbController = new MVCDBController(getApplicationContext());
        treeIntroImageView = (ImageView) findViewById(R.id.tree_intro_image);
        treeIntroTextView = (TextView) findViewById(R.id.tree_intro_description);
        progressBar = (ProgressBar) findViewById(R.id.tree_progress_start);
        right_bar_prb = (ProgressBar) findViewById(R.id.right_bar_prb);
        homeTreeProgressBar = (ProgressBar) findViewById(R.id.tree_progress);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Tree Trails");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fileCache = new FileCache(getApplicationContext());
        imageLoader = new ImageLoader(getApplicationContext());
        try {
            final Drawable upArrow = getResources().getDrawable(abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
            //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        } catch (RuntimeException re) {
            re.getMessage();
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ImageView startButton = (ImageView) findViewById(R.id.tree_intro_start);
        startButton.setOnClickListener(this);
        findViewById(R.id.right_bar_button).setOnClickListener(this);
        treeModel = new TreeModel();
        if (new CheckConnection(getApplicationContext()).isConnectedToInternet()) {
            try {
                displayDefaultTree();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                if (!Utility.isNotNullNotEmptyNotWhiteSpace(AppController.getInstance().getTreeDesc())) {
                    new AlertDialogManager().showAlartDialog(TreeIntroActivity.this, "No Internet", "Error connecting with server. Network may be down. Please try again later.");
                    return;
                }

                treeIntroTextView.setText(AppController.getInstance().getTreeDesc());
                treeIntroImageView.setVisibility(View.VISIBLE);
                homeTreeProgressBar.setVisibility(View.GONE);

                File catchFile = fileCache.getFile(Utility.lastName(AppController.getInstance().getTreeImage()));
                Bitmap bitmap = BitmapFactory.decodeFile(catchFile.getAbsolutePath());
                if (bitmap != null)
                    treeIntroImageView.setImageBitmap(bitmap);
                else {
                    Bitmap bitmapOld = BitmapFactory.decodeResource(getResources(), R.drawable.tree_intro_image);
                    treeIntroImageView.setImageBitmap(bitmapOld);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void displayDefaultTree() {
        try {
            String treeIntroUrl = variables.getBaseUrl() + variables.getGetHomeTreeInfo();
            requestGetCall(treeIntroUrl, variables.getTreeTagObj());
        } catch (Exception ex) {
            ex.getMessage();
            treeIntroImageView.setVisibility(View.VISIBLE);
            homeTreeProgressBar.setVisibility(View.GONE);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.tree_intro_image);
            treeIntroImageView.setImageBitmap(bitmap);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.right_bar_button).setVisibility(View.VISIBLE);
        right_bar_prb.setVisibility(View.GONE);
    }

    public void onClick(View v) {
        if (new CheckConnection(getApplicationContext()).isConnectedToInternet()) {
            if (v.getId() == R.id.tree_intro_start) {
                try {
                    variables.setTreeDataDownloaded(true);
                    String treeGridUrl = variables.getBaseUrl() + variables.getGetAllTree();
                    progressBar.setVisibility(View.VISIBLE);
                    findViewById(R.id.tree_intro_start).setVisibility(View.GONE);
                    requestForTreeGrid(treeGridUrl, variables.getTreeGridTagObj());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (v.getId() == R.id.right_bar_button) {
                if (variables.isTreeDataDownloaded()) {
                    Toast.makeText(TreeIntroActivity.this, "Please wait, tree information is gathering...", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        right_bar_prb.setVisibility(View.VISIBLE);
                        findViewById(R.id.right_bar_button).setVisibility(View.GONE);
                        String treeGridUrl = variables.getBaseUrl() + variables.getGetAllTree();
                        requestForTreeGrid(treeGridUrl, variables.getTreeGridTagObj());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } else {
            if (mvcdbController.isHasAnyTree()) {
                try {
                    Intent intent = new Intent(TreeIntroActivity.this, TreeListActivity.class);
                    startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                new AlertDialogManager().showAlartDialog(TreeIntroActivity.this, "No Internet", "Error connecting with server. Network may be down. Please try again later.");
            }
        }

    }

    public void requestGetCall(String url, String tag) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            runOnMainThread(response);
                        } catch (JSONException ex) {
                            ex.getMessage();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        //Adding policy for socket time out
        RetryPolicy policy = new DefaultRetryPolicy(ConstantsParams.SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag);
    }

    public void requestForTreeGrid(String url, String tag) {
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            runOnMainThreadForTreeGrid(response);
                        } catch (JSONException ex) {
                            ex.getMessage();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                variables.setTreeDataDownloaded(false);
                progressBar.setVisibility(View.GONE);
                right_bar_prb.setVisibility(View.GONE);
                findViewById(R.id.right_bar_button).setVisibility(View.VISIBLE);
                findViewById(R.id.tree_intro_start).setVisibility(View.VISIBLE);
                new AlertDialogManager().showAlartDialog(TreeIntroActivity.this, "Alert!", "There is some problem, please try again!");
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        //Adding policy for socket time out
        RetryPolicy policy = new DefaultRetryPolicy(ConstantsParams.SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag);
    }

    public void runOnMainThread(JSONObject jsonObject) throws JSONException {
        try {
            String HomeTreeImage = jsonObject.getString("HomeTreeImage");
            variables.setHomeTreeID(jsonObject.getString("HomeTreeID"));
            variables.setHomeTreeDesc(jsonObject.getString("HomeTreeDesc"));
            homeTreeProgressBar.setVisibility(View.VISIBLE);
            treeIntroTextView.setText(variables.getHomeTreeDesc());
            AppController.getInstance().setTreeDesc(variables.getHomeTreeDesc());
            //ResizableTextView.makeTextViewResizable(treeIntroTextView, 3, "View More", true);
            imageRequest(this, variables.getImageBaseUrl() + HomeTreeImage, treeIntroImageView);
            imageLoader.queuePhoto(new Variables().getImageBaseUrl() + HomeTreeImage, Utility.lastName(HomeTreeImage));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void runOnMainThreadForTreeGrid(JSONArray jsonArray) throws JSONException {
        try {
            mvcdbController.deleteTree();
        } catch (SQLiteException sqe) {
            sqe.getMessage();
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                TreeListModel treeListModel = new TreeListModel();
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String treeId = jsonObject1.getString("ID");

                if (!mvcdbController.isTreeAlreadyExist(treeId)) {

                    treeListModel.setTreeId(treeId);
                    String treeName = jsonObject1.getString("TreeName");
                    treeListModel.setTreeName(treeName);

                    String treeLocalName = jsonObject1.getString("LocalName");
                    treeListModel.setLocalName(treeLocalName);

                    String treeBotanicalName = jsonObject1.getString("BotanicalName");
                    treeListModel.setBotanicalName(treeBotanicalName);

                    //Tree Details
                    JSONArray customJsonArray = new JSONArray();
                    String desc = jsonObject1.getString("TreeDesc");
                    JSONArray arrayJson = jsonObject1.getJSONArray("TreeImages");

                    for (int treeImgIndex = 0; treeImgIndex < arrayJson.length(); treeImgIndex++) {
                        String imgPath = arrayJson.getString(treeImgIndex);
                        imageLoader.queuePhoto(new Variables().getImageBaseUrl() + imgPath, Utility.lastName(imgPath));
                    }

                    String treeImage = jsonObject1.getString("ThumbnailImage");
                    treeListModel.setTreeGridImageUrl(treeImage);

                    imageLoader.queuePhoto(new Variables().getImageBaseUrl() + treeListModel.getTreeGridImageUrl(), Utility.lastName(treeListModel.getTreeGridImageUrl()));

                    JSONObject customJsonObject = new JSONObject();
                    customJsonObject.put("TreeDesc", desc);
                    customJsonObject.put("TreeImages", arrayJson);
                    customJsonArray.put(customJsonObject);
                    JSONObject treeJsonObject = new JSONObject();
                    treeJsonObject.put("TreesArray", customJsonArray);
                    treeListModel.setTreesArray(treeJsonObject.toString());

                    //Leaf Details
                    JSONArray customJsonArray1 = new JSONArray();
                    String desc1 = jsonObject1.getString("LeafDesc");
                    JSONArray arrayJson1 = jsonObject1.getJSONArray("LeafImages");

                    for (int imgIndex = 0; imgIndex < arrayJson.length(); imgIndex++) {
                        String imgPath = arrayJson1.getString(imgIndex);
                        imageLoader.queuePhoto(new Variables().getImageBaseUrl() + imgPath, Utility.lastName(imgPath));
                    }

                    JSONObject customJsonObject1 = new JSONObject();
                    customJsonObject1.put("LeafDesc", desc1);
                    customJsonObject1.put("LeafImages", arrayJson1);
                    customJsonArray1.put(customJsonObject1);
                    JSONObject leafJsonObject = new JSONObject();
                    leafJsonObject.put("LeafArray", customJsonArray1);
                    treeListModel.setLeafArray(leafJsonObject.toString());

                    //Fruit Details
                    JSONArray customJsonArray2 = new JSONArray();
                    String desc2 = jsonObject1.getString("FruitDesc");
                    JSONArray arrayJson2 = jsonObject1.getJSONArray("FruitImages");
                    for (int imgIndex = 0; imgIndex < arrayJson.length(); imgIndex++) {
                        String imgPath = arrayJson2.getString(imgIndex);
                        imageLoader.queuePhoto(new Variables().getImageBaseUrl() + imgPath, Utility.lastName(imgPath));
                    }
                    JSONObject customJsonObject2 = new JSONObject();
                    customJsonObject2.put("FruitDesc", desc2);
                    customJsonObject2.put("FruitImages", arrayJson2);
                    customJsonArray2.put(customJsonObject2);
                    JSONObject fruitJsonObject = new JSONObject();
                    fruitJsonObject.put("FruitArray", customJsonArray2);
                    treeListModel.setFruitArray(fruitJsonObject.toString());

                    //Flower Details
                    JSONArray customJsonArray3 = new JSONArray();
                    String desc3 = jsonObject1.getString("FlowerDesc");
                    JSONArray arrayJson3 = jsonObject1.getJSONArray("FlowerImages");
                    for (int imgIndex = 0; imgIndex < arrayJson.length(); imgIndex++) {
                        String imgPath = arrayJson3.getString(imgIndex);
                        imageLoader.queuePhoto(new Variables().getImageBaseUrl() + imgPath, Utility.lastName(imgPath));
                    }
                    JSONObject customJsonObject3 = new JSONObject();
                    customJsonObject3.put("FlowerDesc", desc3);
                    customJsonObject3.put("FlowerImages", arrayJson3);
                    customJsonArray3.put(customJsonObject3);
                    JSONObject flowerJsonObject = new JSONObject();
                    flowerJsonObject.put("FlowerArray", customJsonArray3);
                    treeListModel.setFlowerArray(flowerJsonObject.toString());

                    //Bark Details
                    JSONArray customJsonArray4 = new JSONArray();
                    String desc4 = jsonObject1.getString("BarkDesc");
                    JSONArray arrayJson4 = jsonObject1.getJSONArray("BarkImages");
                    for (int imgIndex = 0; imgIndex < arrayJson.length(); imgIndex++) {
                        String imgPath = arrayJson4.getString(imgIndex);
                        imageLoader.queuePhoto(new Variables().getImageBaseUrl() + imgPath, Utility.lastName(imgPath));
                    }
                    JSONObject customJsonObject4 = new JSONObject();
                    customJsonObject4.put("BarkDesc", desc4);
                    customJsonObject4.put("BarkImages", arrayJson4);
                    customJsonArray4.put(customJsonObject4);
                    JSONObject barkJsonObject = new JSONObject();
                    barkJsonObject.put("BarkArray", customJsonArray4);
                    treeListModel.setBarkArray(barkJsonObject.toString());

                    String whichYouLike = jsonObject1.getString("WhichYouLike");
                    treeListModel.setWhichYouLike(whichYouLike);

                    String uniqueIdentifier = jsonObject1.getString("Unique_identifier");
                    treeListModel.setTreeQRCode(uniqueIdentifier);

                    try {
                        mvcdbController.addTree(treeListModel);
                    } catch (SQLiteException ignored) {
                        ignored.printStackTrace();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        try {
            variables.setTreeDataDownloaded(false);
            progressBar.setVisibility(View.GONE);
            findViewById(R.id.tree_intro_start).setVisibility(View.VISIBLE);
            Intent intent = new Intent(TreeIntroActivity.this, TreeListActivity.class);
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void imageRequest(final Activity activity, String imgUrl, final ImageView treeIntroImageView) {
        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        // Initialize a new ImageRequest
        ImageRequest imageRequest = new ImageRequest(
                imgUrl, // Image URL
                new Response.Listener<Bitmap>() { // Bitmap listener
                    @Override
                    public void onResponse(Bitmap response) {
                        // Do something with response
                        treeIntroImageView.setVisibility(View.VISIBLE);
                        homeTreeProgressBar.setVisibility(View.GONE);
                        treeIntroImageView.setImageBitmap(response);
                        treeIntroTextView.setText(variables.getHomeTreeDesc());
                    }
                },
                getScreenWidth(getApplicationContext()), // Image width
                getScreenHeight(getApplicationContext()) / 2, // Image height
                ImageView.ScaleType.CENTER_CROP, // Image scale type
                Bitmap.Config.RGB_565, //Image decode configuration
                new Response.ErrorListener() { // Error listener
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something with error response
                        error.printStackTrace();
                        treeIntroImageView.setVisibility(View.VISIBLE);
                        homeTreeProgressBar.setVisibility(View.GONE);
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.tree_intro_image);
                        treeIntroImageView.setImageBitmap(bitmap);
                        treeIntroTextView.setText(variables.getHomeTreeDesc());
                    }
                }
        );

        //Adding policy for socket time out
        RetryPolicy policy = new DefaultRetryPolicy(ConstantsParams.IMAGE_SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        imageRequest.setRetryPolicy(policy);
        // Add ImageRequest to the RequestQueue
        requestQueue.add(imageRequest);
    }

    public int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public void askForPermission() {
        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestForPermissions();
        } else {
            try {
                //Download images
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void requestForPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT, REQUEST_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Download images
        if (requestCode == REQUEST_CONTACTS) {

        }
    }

}