package com.itravelapp.damdama.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.Placard;

import java.util.Vector;

import android.graphics.Matrix;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

public class ExploreMapActivity extends AppCompatActivity implements OnClickListener{

    RelativeLayout baseView;
    private ScaleGestureDetector scaleGestureDetector;
    private Matrix matrix = new Matrix();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Resort Map");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        baseView = (RelativeLayout)findViewById(R.id.explore_map_baseview);
        baseView.post(new Runnable() {
            @Override
            public void run() {
                calculateCoordinatesForLabels();
            }
        });

        scaleGestureDetector = new ScaleGestureDetector(this,new ScaleListener());

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        System.out.println("On Touch");
        scaleGestureDetector.onTouchEvent(ev);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.
            SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();
            System.out.println("Scale Factor :"+scaleFactor);
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
            matrix.setScale(scaleFactor, scaleFactor);
            ((ImageView)findViewById(R.id.explore_map_imgview)).setImageMatrix(matrix);
            return true;
        }
    }


    public void onClick(View v){
        /*
        if(v.getId() == R.id.game_map_next){

            Intent intent = new Intent(ExploreMapActivity.this, ExploreActivity.class);
            startActivity(intent);
            finish();
        }
        */
    }

    private void calculateCoordinatesForLabels(){

        RelativeLayout baseView = (RelativeLayout)findViewById(R.id.explore_map_baseview);
        final int oldWidth = 1024, oldHeight = 448;
        int availableWidth = baseView.getMeasuredWidth(), availableHeight = baseView.getMeasuredHeight();
        final int newWidth, newHeight;
        if(((float)oldWidth/(float)oldHeight) > ((float)availableWidth/(float)availableHeight)){
            newWidth = availableWidth;
            newHeight = (availableWidth * oldHeight) / oldWidth;
        }else{
            newHeight = availableHeight;
            newWidth = (availableHeight * oldWidth) / oldHeight;
        }

        runOnUiThread(new Runnable() {

            public void run() {
                resizeBaseViewAndAddLabels(newWidth, newHeight, ((float)newWidth / (float)oldWidth));
            }
        });

    }

    private void resizeBaseViewAndAddLabels(int newWidth, int newHeight, float ratio){

        //RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(newWidth,newHeight);//RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        //params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        //baseView.setLayoutParams(params);

        int labelWidth = (int)(ratio * 100);
        int labelHeight = (int)(ratio * 100);

        Vector<Placard> placards = DataManager.getInstance().explorePlacards;

        for(int i = 0 ; i < placards.size() ; i++){

            Placard placard = placards.elementAt(i);

            int oldX = placard.coordX, oldY = placard.coordY;
            int newX = (int)(ratio * oldX);
            int newY = (int)(ratio * oldY);

            int drawX, drawY;
            drawX = newX - (labelWidth / 2) ;
            drawY = newY - labelHeight ;

            //RelativeLayout.LayoutParams labelParams = new RelativeLayout.LayoutParams(labelWidth,labelHeight);//RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            //labelParams.addRule(RelativeLayout.X, drawX);
            //labelParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            //baseView.setLayoutParams(labelParams);

            ImageView labelView = new ImageView(this);
            labelView.setX(drawX);
            labelView.setY(drawY);
            //labelView.setBackgroundResource(R.drawable.checkmark);
            //labelView.setLayoutParams(new RelativeLayout.LayoutParams(labelWidth,labelHeight));
            labelView.setScaleType(ImageView.ScaleType.FIT_XY);
            baseView.addView(labelView, labelWidth, labelHeight);
            labelView.setTag("" + i);
            labelView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    System.gc();

                    int index = Integer.parseInt((String) ((ImageView) v).getTag());

                    DataManager.getInstance().selectedPlacard = DataManager.getInstance().explorePlacards.elementAt(index);

                    Intent intent = new Intent(ExploreMapActivity.this, ExploreActivity.class);
                    startActivity(intent);
                    finish();

                }
            });

        }

    }


}
