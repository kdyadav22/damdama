package com.itravelapp.damdama.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.ExplorePoint;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class RoomDetailActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private Vector<String> mImages = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(DataManager.getInstance().selectedExplorePoint.name);
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.icon_back_small);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_ATOP);
        //upArrow.setColorFilter(Color.parseColor("#FF0000"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        ExplorePoint point = DataManager.getInstance().selectedExplorePoint;

        TextView descriptionLabel = (TextView)findViewById(R.id.room_description_label);
        descriptionLabel.setText(point.description);
        descriptionLabel.setMovementMethod(new ScrollingMovementMethod());

        /*
        try {
            InputStream is = getAssets().open(point.getAllImages().elementAt(0));
            //InputStream is = openFileInput(Utility.getUniqueFileNameFromURL(selectedAudioTour.topViewImageURL));
            Bitmap image = BitmapFactory.decodeStream(is);
            ImageView questionImageView = (ImageView)findViewById(R.id.room_image_view);
            questionImageView.setImageBitmap(image);

        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        mImages = point.getAllImages();

        ViewPager viewPager = (ViewPager) findViewById(R.id.explore_image_pager);
        ImagePagerAdapter adapter = new ImagePagerAdapter();
        viewPager.setOnPageChangeListener(this);
        viewPager.setAdapter(adapter);

        onPageSelected(0);

        TextView roomTypeLabel = (TextView)findViewById(R.id.room_type_label);
        roomTypeLabel.setText(point.name);

    }



    private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            Context context = RoomDetailActivity.this;
            ImageView imageView = new ImageView(context);
            imageView.setBackgroundColor(context.getResources().getColor(R.color.black));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            try {
                InputStream is = getAssets().open(mImages.elementAt(position));
                Bitmap image = BitmapFactory.decodeStream(is);
                imageView.setImageBitmap(image);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //imageView.setImageResource(imageArrayIcon.getResourceId(mImages[position], -1));
            ((ViewPager) container).addView(imageView, 0);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);
        }

        public CharSequence getPageTitle (int position) {
            return "Your static title";
        }
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // TODO Auto-generated method stub

//        titleLabel.setText(imageTitles[position]);
//        descLabel.setText(imagesDescs[position]);
    }

    @Override
    public void onPageSelected(int position) {
        // TODO Auto-generated method stub

//        titleLabel.setText(imageTitles[position]);
//        descLabel.setText(imagesDescs[position]);

    }

}
