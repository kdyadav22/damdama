package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.parsers.ExplorePointsParser;
import com.itravelapp.damdama.views.NavigationCellView;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    Activity currentActivity = null;
    ListView listView = null;
    ListCellAdapter listCellAdapter = null;
    TextView titleLabel, descLabel;
    private int[] mImages = new int[]{R.drawable.main_bg_1_small, R.drawable.main_bg_2_small, R.drawable.main_bg_3_small, R.drawable.main_bg_4_small, R.drawable.main_bg_5_small};
    private String[] imageTitles = new String[]{"WELCOME", "RESTAURANTS", "COMFORTABLE ROOMS", "REJUVENATING SPA", "WHERE WE ARE"};
    private String[] imagesDescs = new String[]{"Come experience nature woven beautifully in the first ever Gateway resort in India. Derived from the existing landscape, The Gateway Resort Damdama Lake Gurgaon welcomes you to a nature inspired sanctuary for the urban nomad.",
            "Discover Gurgaon through its culinary treats. At The Gateway Resort Damdama Lake, you’ll find the choicest dining options in Gurgaon. From hearty north Indian traditional meals to a delectable selection of international favorites to healthy active foods.",
            "Whether you're on a holiday or a business trip, take a breather and relax at our holistic wellness spa. From relaxation massages to grooming treatments, our spa offer a wide selection of therapies to choose from.",
            "Whether you're on a holiday or a business trip, take a breather and relax at our holistic wellness spa. From relaxation massages to grooming treatments, our spa offer a wide selection of therapies to choose from.",
            "PO Damdama, Off Sohna-Gurgaon Rd Gurgaon Haryana India 122102\nEmail: gateway.damdama@tajhotels.com\nPhone: +91 124 398 3000\nFax: +91 124 398 3001"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Welcome");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);
        currentActivity = this;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        listView = (ListView) findViewById(R.id.navList);
        listCellAdapter = new ListCellAdapter();
        listView.setOnItemClickListener(didSelectedListCell);
        setAdapter();
        titleLabel = (TextView) findViewById(R.id.main_image_title_label);
        descLabel = (TextView) findViewById(R.id.main_image_desc_label);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ImagePagerAdapter adapter = new ImagePagerAdapter();
        viewPager.setOnPageChangeListener(this);
        viewPager.setAdapter(adapter);
        onPageSelected(0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAdapter() {
        if (listView.getAdapter() == null && listCellAdapter != null) {
            listView.setAdapter(listCellAdapter);
        }
        listCellAdapter.notifyDataSetChanged();
    }

    public void populateView(View view, int position) {

        ViewHolder holder = (ViewHolder) view.getTag();
        String item = "";
        int imageID = R.drawable.nav_icon_1;

        if (position == 0) {
            item = "Directions";
            imageID = R.drawable.nav_icon_1;
        } else if (position == 1) {
            item = "Treasure Hunt";
            imageID = R.drawable.nav_icon_2;
        } else if (position == 2) {
            item = "Explore";
            imageID = R.drawable.nav_icon_3;
        } else if (position == 3) {
            item = "Tree Trail";
            imageID = R.drawable.nav_icon_4;
        } else if (position == 4) {
            item = "Events";
            imageID = R.drawable.nav_icon_5;
        }

        holder.titleView.setText(item);
        holder.thumbnailView.setImageResource(imageID);
    }

    public void loadExploreData() {

        try {
            InputStream is = getApplicationContext().getAssets().open("explore.xml");
            ExplorePointsParser parser = new ExplorePointsParser();
            parser.parseLocalXmlFile(is);
            DataManager.getInstance().explorePlacards = parser.dataList;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("MohitError: Error in explore.xml");
            showAlert("Error in explore.xml\n" + e.getMessage());
            //errorWhileRecievingData("Error in locations.xml\n" + e.getMessage());
        }
    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private OnItemClickListener didSelectedListCell = new OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int index,
                                long arg3) {

            if (index == 0) {
                Intent intent = new Intent(MainActivity.this, MapListActivity.class);
                startActivity(intent);
            } else if (index == 1) {
                Intent intent = new Intent(MainActivity.this, GameIntroActivity.class);
                startActivity(intent);
            } else if (index == 2) {
                if (DataManager.getInstance().explorePlacards == null)
                    loadExploreData();
                Intent intent = new Intent(MainActivity.this, ExploreListActivity.class);
                startActivity(intent);
            } else if (index == 3) {
                Intent intent = new Intent(MainActivity.this, TreeIntroActivity.class);
                startActivity(intent);
            } else if (index == 4) {
                Intent intent = new Intent(MainActivity.this, EventIntroActivity.class);
                startActivity(intent);
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
    };

    class ListCellAdapter extends BaseAdapter {

        public int getCount() {

            return 5;//dataList.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflateView();
            }
            populateView(convertView, position);
            return convertView;
        }

        private View inflateView() {

            NavigationCellView cellView = new NavigationCellView(currentActivity);

            ViewHolder vh = new ViewHolder();
            vh.thumbnailView = cellView.thumbnailView;
            vh.titleView = cellView.titleView;

            cellView.view.setTag(vh);

            return cellView.view;
        }
    }

    private static class ViewHolder {
        ImageView thumbnailView;
        TextView titleView;
    }

    private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Context context = MainActivity.this;
            ImageView imageView = new ImageView(context);
            imageView.setBackgroundColor(context.getResources().getColor(R.color.black));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageResource(mImages[position]);
            (container).addView(imageView, 0);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);
        }

        public CharSequence getPageTitle(int position) {
            return "Your static title";
        }
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int position) {
        // TODO Auto-generated method stub

        titleLabel.setText(imageTitles[position]);
        descLabel.setText(imagesDescs[position]);

    }
}
