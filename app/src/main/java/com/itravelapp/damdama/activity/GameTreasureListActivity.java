package com.itravelapp.damdama.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.managers.DataManager;
import com.itravelapp.damdama.models.TreasurePoint;
import com.itravelapp.damdama.views.TreasureListCellView;

import java.util.Date;
import java.util.Vector;

public class GameTreasureListActivity extends AppCompatActivity  implements View.OnClickListener {

    Activity currentActivity  = null;

    ListView listView = null;
    ListCellAdapter listCellAdapter = null;

    Boolean shouldStopThread = false;
    TextView timeLabel;

    Vector<TreasurePoint> dataList = null;
    Boolean hasTimeOver = false;

    Thread updateLabelThread = null;
    Boolean timeOverAlertShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_treasure_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Treasure List");
        toolbar.setTitleTextAppearance(this, R.style.NavBarTitle);
        toolbar.setSubtitleTextAppearance(this, R.style.NavBarSubTitle);
        setSupportActionBar(toolbar);

        currentActivity = this;

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        timeLabel = (TextView)findViewById(R.id.treasure_list_time_label);

        ((TextView)findViewById(R.id.treasure_list_done_button)).setOnClickListener(this);

        dataList = DataManager.getInstance().treasurePointsList;

        listView = (ListView)findViewById(R.id.treasure_list);
        listCellAdapter = new ListCellAdapter();
        listView.setOnItemClickListener(didSelectedListCell);
        setAdapter();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        shouldStopThread = false;
        updateLabelThread = new Thread(new Runnable() {
            public void run() {
                System.out.println("check");
                while (shouldStopThread == false) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    updateTimeLabel();
                }
            }
        });
        updateLabelThread.start();

        setAdapter();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        shouldStopThread = true;
    }

    private void updateTimeLabel(){

        runOnUiThread(new Runnable() {

            public void run() {
                Date currentDateTime = new Date();
                long duration = (currentDateTime.getTime() - DataManager.getInstance().gameStartTime.getTime());
                if(duration >= DataManager.getInstance().maxGameTime){

                    shouldStopThread = true;
                    hasTimeOver = true;
                    timeLabel.setText("Time over");
                    if(timeOverAlertShown == false) {
                        timeOverAlertShown = true;
                        showAlert("Time is Over!");
                    }

                }else {
                    timeLabel.setText(formatDurationToString(duration));
                }
            }
        });

    }

    public String formatDurationToString(long durationInMillis){

        int durationInSecs = (int)(durationInMillis / 1000);
        int hours = durationInSecs / 3600;
        int minutes = durationInSecs / 60;
        int secs = durationInSecs % 60;

        return String.format("%02d:%02d:%02d",hours,minutes,secs);
    }

    public void onClick(View v){

        if(v.getId() == R.id.treasure_list_done_button){

            if(hasTimeOver){
                Intent intent = new Intent(GameTreasureListActivity.this, GameCertificateActivity.class);
                startActivity(intent);
                finish();
                System.gc();
                return;
            }

            Boolean isRemaining = false;
            for (int i = 0 ; i < DataManager.getInstance().completedList.size(); i++){

                int status = DataManager.getInstance().completedList.elementAt(i);
                if(status == 0){
                    isRemaining = true;
                    break;
                }

            }

            if(isRemaining){
                this.runOnUiThread(new Runnable() {

                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(GameTreasureListActivity.this);
                        builder.setMessage("It will end the game. Do you want to continue?")
                                .setCancelable(false)
                                .setPositiveButton("No",
                                        new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                Intent intent = new Intent(GameTreasureListActivity.this, GameCertificateActivity.class);
                                                startActivity(intent);
                                                finish();
                                                System.gc();
                                                return;
                                            }
                                        });
                        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });

                return;
            }

            this.runOnUiThread(new Runnable() {

                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(GameTreasureListActivity.this);
                    builder.setMessage("This will end the game. Do you want to re-try the skipped points?")
                            .setCancelable(false)
                            .setPositiveButton("No",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    Intent intent = new Intent(GameTreasureListActivity.this, GameCertificateActivity.class);
                                    startActivity(intent);
                                    finish();
                                    System.gc();
                                    return;
                                }
                            });
                    builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });


        }

    }


    public void setAdapter() {

        if (listView.getAdapter() == null && listCellAdapter != null) {
            listView.setAdapter(listCellAdapter);
        }
        listCellAdapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener didSelectedListCell = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> arg0, View arg1, int index,
                                long arg3) {

            if(hasTimeOver){
                showAlert("Time is over. Click on Next to see your result.");
                return;
            }

            Integer status = DataManager.getInstance().completedList.elementAt(index);
            if(status.intValue() == 0) {

                Intent intent = new Intent(GameTreasureListActivity.this, GameQuestionActivity.class);
                intent.putExtra("CURRENT_INDEX", index);
                intent.putExtra("FROM_TREASURELIST", true);
                startActivity(intent);
            }else{

                showAlert("You have already found this treasure.");
            }

        }

    };

    public void populateView(View view, int position) {

        ViewHolder holder = (ViewHolder) view.getTag();
        String item = "Treasure Point " + (position + 1);

        TreasurePoint point = dataList.elementAt(position);
        Integer status = DataManager.getInstance().completedList.elementAt(position);

        int imageID = R.drawable.checkmark;

        if(status.intValue() == 1){
            item = point.name;
            imageID = R.drawable.checkmark_sel;
        }

        holder.titleView.setText(item);
        holder.thumbnailView.setImageResource(imageID);

    }

    class ListCellAdapter extends BaseAdapter {

        public int getCount() {

            if(dataList != null)
                return dataList.size();

            return  0;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflateView();
            }
            populateView(convertView, position);
            return convertView;
        }

        private View inflateView() {

            TreasureListCellView cellView = new TreasureListCellView(currentActivity);

            ViewHolder vh = new ViewHolder();
            vh.thumbnailView = cellView.thumbnailView;
            vh.titleView = cellView.titleView;

            cellView.view.setTag(vh);

            return cellView.view;

        }

    }

    private static class ViewHolder {

        ImageView thumbnailView;
        TextView titleView;

    }

    public void showAlert(final String msg) {
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameTreasureListActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

}
