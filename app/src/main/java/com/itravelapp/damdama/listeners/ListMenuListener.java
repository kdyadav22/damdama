package com.itravelapp.damdama.listeners;

import android.view.View;

/**
 * Created by kapilyadav on 4/7/2017.
 */

public interface ListMenuListener {
    void onMenuClick(View view, String planId, String position);
}

