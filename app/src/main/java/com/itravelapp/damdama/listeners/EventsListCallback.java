package com.itravelapp.damdama.listeners;

/**
 * Created by ${Kapil} on 22/10/17.
 */

public interface EventsListCallback {
    void onClick(int pos);
}
