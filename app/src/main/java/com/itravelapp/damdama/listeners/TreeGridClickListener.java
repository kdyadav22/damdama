package com.itravelapp.damdama.listeners;

/**
 * Created by ${Kapil} on 16/10/17.
 */

public interface TreeGridClickListener {
    void onGridClick(int pos);
}
