package com.itravelapp.damdama.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.itravelapp.damdama.R;
import com.itravelapp.damdama.activity.TreeDetailActivity;
import com.itravelapp.damdama.imageloader.FileCache;
import com.itravelapp.damdama.logger.Log;
import com.itravelapp.damdama.models.TreeListModel;
import com.itravelapp.damdama.utility.Utility;
import com.itravelapp.damdama.utility.Variables;
import com.itravelapp.damdama.volley.ConstantsParams;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ${Kapil} on 16/10/17.
 */

public class TreeItemAdapter extends RecyclerView.Adapter<TreeItemViewHolder> {

    private ArrayList<TreeListModel> treeListModels;
    private Activity activity;
    private FileCache fileCache;

    public TreeItemAdapter(Activity activity, ArrayList<TreeListModel> treeListModels) {
        this.activity = activity;
        this.treeListModels = treeListModels;
        fileCache = new FileCache(activity);
    }

    @Override
    public TreeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cell_treelist_item, parent, false);

        return new TreeItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TreeItemViewHolder holder, final int position) {
        final TreeListModel treeListModel = treeListModels.get(position);
        holder.treesName.setText(treeListModel.getTreeName());

        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
        final int width = displayMetrics.widthPixels / 3;
        final int height = displayMetrics.widthPixels / 3;
        ViewGroup.LayoutParams layoutParams = holder.rowLayout.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;

        try {
            File catchFile = fileCache.getFile(Utility.lastName(treeListModel.getTreeGridImageUrl()));
            String imagePath = catchFile.getAbsolutePath();
            displayImage(holder.treesImage, imagePath, width, new Variables().getImageBaseUrl() + treeListModel.getTreeGridImageUrl());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        holder.rowLayout.setLayoutParams(layoutParams);

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (treeGridClickListener != null)
                    treeGridClickListener.onGridClick(position);*/
                try {
                    Intent intent = new Intent(activity, TreeDetailActivity.class);
                    intent.putExtra("TreeName", treeListModel.getTreeName());
                    intent.putExtra("TreeID", treeListModel.getTreeId());
                    activity.startActivity(intent);
                    //etSearchTree.setText("");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return treeListModels.size();
    }

    public void removeData(int position) {
        treeListModels.remove(position);
        notifyItemRemoved(position);
    }

    private void displayImage(final ImageView imageView, final String path, final int size, final String imgUrl) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap = BitmapFactory.decodeFile(path);
                Log.d("Bitmap: ", "Bitmap: " + bitmap);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(bitmap!=null)
                        imageView.setImageBitmap(bitmap);
                        else{
                            imageRequest(activity, imgUrl, imageView, size);
                        }
                    }
                });
            }
        }).start();
    }

    private void imageRequest(final Activity activity, String imgUrl, final ImageView treeIntroImageView, final int size) {
        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        // Initialize a new ImageRequest
        ImageRequest imageRequest = new ImageRequest(
                imgUrl, // Image URL
                new Response.Listener<Bitmap>() { // Bitmap listener
                    @Override
                    public void onResponse(Bitmap response) {
                        // Do something with response
                        treeIntroImageView.setVisibility(View.VISIBLE);
                        treeIntroImageView.setImageBitmap(response);
                    }
                },
                size, // Image width
                size, // Image height
                ImageView.ScaleType.CENTER_CROP, // Image scale type
                Bitmap.Config.RGB_565, //Image decode configuration
                new Response.ErrorListener() { // Error listener
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something with error response
                        error.printStackTrace();
                        treeIntroImageView.setVisibility(View.VISIBLE);
                        //treeIntroImageView.setImageBitmap(response);
                    }
                }
        );

        //Adding policy for socket time out
        RetryPolicy policy = new DefaultRetryPolicy(ConstantsParams.IMAGE_SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        imageRequest.setRetryPolicy(policy);
        // Add ImageRequest to the RequestQueue
        requestQueue.add(imageRequest);
    }
}
