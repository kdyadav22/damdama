package com.itravelapp.damdama.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.itravelapp.damdama.R;

/**
 * Created by ${Kapil} on 22/10/17.
 */

public class EventItemViewHolder extends RecyclerView.ViewHolder {
    RelativeLayout relativeLayout;
    ImageView eventsBackgroundImageView;
    TextView eventsNameTextView;
    TextView eventsDurationTextView;

    public EventItemViewHolder(View itemView) {
        super(itemView);
        relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rl_rowLayout);
        eventsBackgroundImageView = (ImageView)itemView.findViewById(R.id.iv_events_background);
        eventsNameTextView = (TextView)itemView.findViewById(R.id.tv_events_name);
        eventsDurationTextView = (TextView)itemView.findViewById(R.id.tv_events_duration);
    }
}
