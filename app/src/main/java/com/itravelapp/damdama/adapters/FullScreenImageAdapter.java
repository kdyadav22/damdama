package com.itravelapp.damdama.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.custom_imageview.TouchImageView;
import com.itravelapp.damdama.imageloader.FileCache;
import com.itravelapp.damdama.utility.Utility;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Kapil on 08/11/17.
 */

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<String> _imagePaths;
    FileCache fileCache;

    // constructor
    public FullScreenImageAdapter(Activity activity,
                                  ArrayList<String> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
        fileCache = new FileCache(activity);
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        Button btnClose;
        LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.view_cell_full_image, container, false);
        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        String imagePath = _imagePaths.get(position);
        imgDisplay.setBackgroundColor(-_activity.getResources().getColor(R.color.black));
        try {
            File catchFile = fileCache.getFile(Utility.lastName(imagePath));
            //Bitmap bitmap = BitmapFactory.decodeFile(catchFile.getAbsolutePath());
            imgDisplay.setImageBitmap(BitmapFactory.decodeFile(catchFile.getAbsolutePath()));

        /*Bitmap bitmap = BitmapScaler.scaleToFitHeight(BitmapFactory.decodeFile(catchFile.getAbsolutePath()), getScreenHeight(_activity)-120);
        imgDisplay.setImageBitmap(bitmap);*/

            // close button click event
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _activity.finish();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        (container).addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        (container).removeView((RelativeLayout) object);

    }

    public int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }
}
