package com.itravelapp.damdama.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.models.EventsModel;
import com.itravelapp.damdama.utility.StaticConstants;

import java.util.ArrayList;

/**
 * Created by ${Kapil} on 22/10/17.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventItemViewHolder> {
    Activity activity;
    ArrayList<EventsModel> eventsModelArrayList;

    public EventsAdapter(Activity activity, ArrayList<EventsModel> eventsModelArrayList) {
        this.activity = activity;
        this.eventsModelArrayList = eventsModelArrayList;
    }

    @Override
    public EventItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cell_events_item, parent, false);
        return new EventItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventItemViewHolder holder, final int position) {
        EventsModel eventsModel = eventsModelArrayList.get(position);
        holder.eventsNameTextView.setText(eventsModel.getEventName());
        holder.eventsDurationTextView.setText(eventsModel.getEventStartDate() + " - " + eventsModel.getEventEndDate());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticConstants.eventsListCallback.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventsModelArrayList.size();
    }
}
