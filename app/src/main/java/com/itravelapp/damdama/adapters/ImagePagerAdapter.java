package com.itravelapp.damdama.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.activity.FullScreenImage;
import com.itravelapp.damdama.imageloader.FileCache;
import com.itravelapp.damdama.utility.Utility;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Panalink-03 on 11/20/2017.
 */


public class ImagePagerAdapter extends PagerAdapter {
    String[] mImages;
    Activity activity;
    ArrayList<String> _imagesPath;
    FileCache fileCache;

    public ImagePagerAdapter(Activity activity, String[] mImages, ArrayList<String> _imagesPath) {
        this.activity = activity;
        this.mImages = mImages;
        this._imagesPath = _imagesPath;
        fileCache = new FileCache(activity);
    }

    @Override
    public int getCount() {
        return mImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Context context = activity;
        final String imagePath = mImages[position];
        ImageView imageView;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.viewpager_image_item, container, false);
        imageView = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        imageView.setBackgroundColor(context.getResources().getColor(R.color.black));
        try {
            File catchFile = fileCache.getFile(Utility.lastName(imagePath));
            //Bitmap bitmap = BitmapFactory.decodeFile(catchFile.getAbsolutePath());
            imageView.setImageBitmap(BitmapFactory.decodeFile(catchFile.getAbsolutePath()));
            //imageView.setImageBitmap(com.itravelapp.damdama.custom_imageview.BitmapScaler.scaleToFitHeight(BitmapFactory.decodeFile(catchFile.getAbsolutePath()), getScreenHeight(getActivity())-(p24+p25+p26+10)));

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (_imagesPath.size() > 0) {
                        Intent intent = new Intent(activity, FullScreenImage.class);
                        intent.putExtra("ImageArray", _imagesPath);
                        intent.putExtra("Position", position);
                        activity.startActivity(intent);
                    }else{
                        Toast.makeText(activity, "No Image Found!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        container.addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }

    public CharSequence getPageTitle(int position) {
        return "Your static title";
    }
}
