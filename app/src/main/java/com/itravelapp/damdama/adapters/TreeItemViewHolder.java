package com.itravelapp.damdama.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itravelapp.damdama.R;


/**
 * Created by ${Kapil} on 16/10/17.
 */

public class TreeItemViewHolder extends RecyclerView.ViewHolder {
    public FrameLayout rowLayout;
    public ImageView treesImage;
    public TextView treesName;

    public TreeItemViewHolder(View itemView) {
        super(itemView);
        rowLayout = (FrameLayout) itemView.findViewById(R.id.ll_treeRowLayout);
        treesImage = (ImageView) itemView.findViewById(R.id.iv_treeitem);
        treesName = (TextView) itemView.findViewById(R.id.tv_treename);
    }
}
