package com.itravelapp.damdama.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.activity.TreeDetailActivity;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.models.TreeListModel;
import com.itravelapp.damdama.utility.Variables;
import com.itravelapp.damdama.volley.AppController;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment implements View.OnClickListener {
    //Fragment Declaration
    Fragment fragment;
    FragmentManager fragmentManager;
    public int SCANNER_REQUEST_CODE = 123;
    Bundle bundle;
    MVCDBController mvcdbController;
    String treeId;

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        bundle = getArguments();
        treeId = bundle.getString("TreeID");
        fragmentManager = getFragmentManager();
        mvcdbController = new MVCDBController(getActivity());
        view.findViewById(R.id.barkButton).setOnClickListener(this);
        view.findViewById(R.id.triviaButton).setOnClickListener(this);
        view.findViewById(R.id.scanButton).setOnClickListener(this);
        view.findViewById(R.id.shareButton).setOnClickListener(this);
        AppController.getInstance().setMoreButtons(false);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.barkButton:
                try {
                    AppController.getInstance().setMoreButtons(true);
                    Bundle bundle = new Bundle();
                    bundle.putString("TreeID", treeId);
                    fragment = new BarkFragment();
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case R.id.triviaButton:
                try {
                    AppController.getInstance().setMoreButtons(true);
                    Bundle bundle = new Bundle();
                    bundle.putString("TreeID", treeId);
                    fragment = new IntrestingTriviaFragment();
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case R.id.scanButton:
                try{
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "SCAN_MODE");
                startActivityForResult(intent, SCANNER_REQUEST_CODE);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case R.id.shareButton:
                try {
                    TreeListModel treeListModel = mvcdbController.getTreeFromId(treeId);
                    String treeName = treeListModel.getTreeName();
                    String treeImageUrl = new Variables().getImageBaseUrl() + treeListModel.getTreeGridImageUrl();
                    shareOnMedia(treeName, treeImageUrl);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }

    public void shareOnMedia(String shareText, String imageUrl) {
        try{
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(shareText) + "\n" + imageUrl);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share your tree:"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        if (requestCode == SCANNER_REQUEST_CODE) {
            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                try{
                String contents = intent.getStringExtra("SCAN_RESULT");
                Log.d("Scan content", "Scan content: " + contents);
                Intent scanIntent1 = new Intent(getActivity(), TreeDetailActivity.class);
                /*scanIntent1.putExtra("QR_Content", contents);
                scanIntent1.putExtra("IsFromScan", true);*/
                scanIntent1.putExtra("TreeName", mvcdbController.getTreeName(contents));
                scanIntent1.putExtra("TreeID", mvcdbController.getTreeId(contents));
                startActivity(scanIntent1);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
                Toast toast = Toast.makeText(getActivity(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    }
}
