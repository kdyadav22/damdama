package com.itravelapp.damdama.fragment;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.itravelapp.damdama.R;
import com.itravelapp.damdama.activity.TreeDetailActivity;
import com.itravelapp.damdama.activity.TreeListActivity;
import com.itravelapp.damdama.adapters.TreeItemAdapter;
import com.itravelapp.damdama.adapters.TreeItemViewHolder;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.listeners.TreeGridClickListener;
import com.itravelapp.damdama.models.TreeListModel;
import com.itravelapp.damdama.util.ImageCache;
import com.itravelapp.damdama.util.ImageFetcher;
import com.itravelapp.damdama.util.Utils;
import com.itravelapp.damdama.utility.StaticConstants;
import com.itravelapp.damdama.utility.Variables;
import com.itravelapp.damdama.volley.ConstantsParams;

import java.util.ArrayList;

import static android.widget.AbsListView.*;

/**
 * A simple {@link Fragment} subclass.
 */

public class TreeListFragment extends Fragment implements TreeGridClickListener {
    RecyclerView recyclerView;
    MVCDBController mvcdbController;
    ArrayList<TreeListModel> arrayList = new ArrayList<>();

    private static final String TAG = "ImageGridFragment";
    private static final String IMAGE_CACHE_DIR = "thumbs";

    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private ImageFetcher mImageFetcher;

    public TreeListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = inflater.inflate(R.layout.fragment_tree_list, container, false);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(getActivity(), IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getActivity(), mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(getActivity().getSupportFragmentManager(), cacheParams);

        mvcdbController = new MVCDBController(getActivity());
        StaticConstants.treeGridClickListener = this;
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_treeRecyclerView);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        arrayList = mvcdbController.getAllTreeList();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            recyclerView.setOnScrollChangeListener(new OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                    if (!Utils.hasHoneycomb()) {
                        mImageFetcher.setPauseWork(true);
                    }
                }
            });
        } else {
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    if (!Utils.hasHoneycomb()) {
                        mImageFetcher.setPauseWork(true);
                    }
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (!Utils.hasHoneycomb()) {
                        mImageFetcher.setPauseWork(true);
                    }
                    super.onScrolled(recyclerView, dx, dy);
                }
            });
        }

        return view;
    }

    @Override
    public void onGridClick(int pos) {
        Intent intent = new Intent(getActivity(), TreeDetailActivity.class);
        intent.putExtra("TreeID", arrayList.get(pos).getTreeId());
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            if (arrayList.size() > 0)
                recyclerView.setAdapter(new TreeItemAdapter(getActivity(), arrayList));
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public class TreeItemAdapter extends RecyclerView.Adapter<TreeItemViewHolder> {

        private ArrayList<TreeListModel> treeListModels;
        private Activity activity;

        public TreeItemAdapter(Activity activity, ArrayList<TreeListModel> treeListModels) {
            this.activity = activity;
            this.treeListModels = treeListModels;
        }

        @Override
        public TreeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cell_treelist_item, parent, false);

            return new TreeItemViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(TreeItemViewHolder holder, final int position) {
            TreeListModel treeListModel = treeListModels.get(position);
            holder.treesName.setText(treeListModel.getTreeName());

            //imageRequest(activity, new Variables().getImageBaseUrl()+treeListModel.getTreeGridImageUrl(), holder.treesImage);

            DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
            final int width = displayMetrics.widthPixels / 3;
            ViewGroup.LayoutParams layoutParams = holder.rowLayout.getLayoutParams();
            layoutParams.width = width;
            layoutParams.height = width;
            holder.rowLayout.setLayoutParams(layoutParams);

            mImageFetcher.loadImage(new Variables().getImageBaseUrl() + treeListModel.getTreeGridImageUrl(), holder.treesImage);

            holder.rowLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StaticConstants.treeGridClickListener.onGridClick(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return treeListModels.size();
        }

        public void removeData(int position) {
            treeListModels.remove(position);
            notifyItemRemoved(position);
        }

        public void imageRequest(final Activity activity, String imgUrl, final ImageView treeIntroImageView) {
            // Initialize a new RequestQueue instance
            RequestQueue requestQueue = Volley.newRequestQueue(activity);

            // Initialize a new ImageRequest
            ImageRequest imageRequest = new ImageRequest(
                    imgUrl, // Image URL
                    new Response.Listener<Bitmap>() { // Bitmap listener
                        @Override
                        public void onResponse(Bitmap response) {
                            // Do something with response
                            treeIntroImageView.setVisibility(View.VISIBLE);
                            treeIntroImageView.setImageBitmap(response);
                        }
                    },
                    0, // Image width
                    0, // Image height
                    ImageView.ScaleType.CENTER_CROP, // Image scale type
                    Bitmap.Config.RGB_565, //Image decode configuration
                    new Response.ErrorListener() { // Error listener
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Do something with error response
                            error.printStackTrace();
                            treeIntroImageView.setVisibility(View.VISIBLE);
                            Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.tree_intro_image);
                            treeIntroImageView.setImageBitmap(bitmap);
                        }
                    }
            );

            //Adding policy for socket time out
            RetryPolicy policy = new DefaultRetryPolicy(ConstantsParams.IMAGE_SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            imageRequest.setRetryPolicy(policy);
            // Add ImageRequest to the RequestQueue
            requestQueue.add(imageRequest);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        (recyclerView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageFetcher.setPauseWork(false);
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }
}
