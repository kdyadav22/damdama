package com.itravelapp.damdama.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.activity.TreeDetailActivity;
import com.itravelapp.damdama.adapters.ImagePagerAdapter;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.imageloader.FileCache;
import com.itravelapp.damdama.models.TreeListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeafFragment extends Fragment implements ViewPager.OnPageChangeListener {
    TreeListModel treeListModel = new TreeListModel();
    ViewPager viewPager;
    private String[] mImages;
    Bundle bundle;
    FileCache fileCache;
    MVCDBController mvcdbController;
    ArrayList<String> _imagesPath = new ArrayList<>();

    public LeafFragment() {
        // Required empty public constructor
    }

    LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    ImagePagerAdapter mAdapter;
    int p24 = TreeDetailActivity.getInstance().statusBarHeight;
    int p25 = TreeDetailActivity.getInstance().actionBarHeight;
    int p26 = TreeDetailActivity.getInstance().navigationBarHeight;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tree_details, container, false);
        bundle = getArguments();
        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        fileCache = new FileCache(getActivity());
        mvcdbController = new MVCDBController(getActivity());
        treeListModel = mvcdbController.getTreeFromId(bundle.getString("TreeID"));
        try {
            JSONObject jsonObject = new JSONObject(treeListModel.getLeafArray());
            JSONArray jsonArray = jsonObject.getJSONArray("LeafArray");
            Log.d("JArr", "JArr " + jsonArray.length());
            String treeDesc = jsonArray.getJSONObject(0).getString("LeafDesc");
            ((TextView) view.findViewById(R.id.tree_detail_description)).setText(treeDesc);
            //ResizableTextView.makeTextViewResizable((TextView) view.findViewById(R.id.tree_detail_description), R.integer.maxLine,"View More", true);
            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
            JSONArray treeArrayJson = (JSONArray) jsonObject1.get("LeafImages");
            mImages = new String[treeArrayJson.length()];
            for (int i = 0; i < treeArrayJson.length(); i++) {
                String imgPath = treeArrayJson.getString(i);
                mImages[i] = imgPath;
                _imagesPath.add(imgPath);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {
        } catch (Exception ex) {
        }

        viewPager = (ViewPager) view.findViewById(R.id.tree_detail_pager);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.height = getScreenHeight(getActivity()) - (p24 + p25 + p26 + 10); //left, top, right, bottom
        viewPager.setLayoutParams(params);
        if(mImages.length>0) {
            try{
                mAdapter = new ImagePagerAdapter(getActivity(), mImages, _imagesPath);
            viewPager.setAdapter(mAdapter);
            viewPager.setCurrentItem(0);
            viewPager.setOnPageChangeListener(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
            setUiPageViewController();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return view;
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onPageSelected(int position) {
        // TODO Auto-generated method stub
        Log.d("###onPageSelected, pos ", String.valueOf(position));
        try{
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselected_itemdots));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dots));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setUiPageViewController() {
        try {
            dotsCount = mAdapter.getCount();
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselected_itemdots));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                pager_indicator.addView(dots[i], params);
            }

            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dots));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }
}
