package com.itravelapp.damdama.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.activity.TreeDetailActivity;
import com.itravelapp.damdama.adapters.ImagePagerAdapter;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.imageloader.FileCache;
import com.itravelapp.damdama.models.TreeListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TreeDetails extends Fragment implements ViewPager.OnPageChangeListener {
    TreeListModel treeListModel = new TreeListModel();
    ViewPager viewPager;
    private String[] mImages;
    ArrayList<String> _imagesPath = new ArrayList<>();
    Bundle bundle;
    MVCDBController mvcdbController;
    FileCache fileCache;
    LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    ImagePagerAdapter mAdapter;
    JSONObject jsonObject1;

    public TreeDetails() {
        // Required empty public constructor
    }

    int p24 = TreeDetailActivity.getInstance().statusBarHeight;
    int p25 = TreeDetailActivity.getInstance().actionBarHeight;
    int p26 = TreeDetailActivity.getInstance().navigationBarHeight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tree_details, container, false);
        bundle = getArguments();
        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        mvcdbController = new MVCDBController(getActivity());
        fileCache = new FileCache(getActivity());

        treeListModel = mvcdbController.getTreeFromId(bundle.getString("TreeID"));
        try {
            JSONObject jsonObject = new JSONObject(treeListModel.getTreesArray());
            JSONArray jsonArray = jsonObject.getJSONArray("TreesArray");
            Log.d("JArr", "JArr " + jsonArray.length());
            String treeDesc = jsonArray.getJSONObject(0).getString("TreeDesc");
            ((TextView) view.findViewById(R.id.tree_detail_description)).setText(treeDesc);
            //ResizableTextView.makeTextViewResizable((TextView) view.findViewById(R.id.tree_detail_description), 5, "View More", true);
            jsonObject1 = jsonArray.getJSONObject(0);
            JSONArray treeArrayJson = (JSONArray) jsonObject1.get("TreeImages");
            mImages = new String[treeArrayJson.length()];
            for (int i = 0; i < treeArrayJson.length(); i++) {
                String imgPath = treeArrayJson.getString(i);
                mImages[i] = imgPath;
                _imagesPath.add(imgPath);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {
        } catch (Exception ex) {
        }

        viewPager = (ViewPager) view.findViewById(R.id.tree_detail_pager);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.height = getScreenHeight(getActivity()) - (p24 + p25 + p26 + 10); //left, top, right, bottom
        viewPager.setLayoutParams(params);

        if (mImages.length > 0) {
            try {
                mAdapter = new ImagePagerAdapter(getActivity(), mImages, _imagesPath);
                viewPager.setAdapter(mAdapter);
                viewPager.setCurrentItem(0);
                viewPager.setOnPageChangeListener(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                setUiPageViewController();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return view;

    }

    /*private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            Context context = getActivity();
            String imagePath = mImages[position];
            ImageView imageView;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View viewLayout = inflater.inflate(R.layout.viewpager_image_item, container, false);
            imageView = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
            imageView.setBackgroundColor(context.getResources().getColor(R.color.black));
            try {
                File catchFile = fileCache.getFile(Utility.lastName(imagePath));
                //Bitmap bitmap = BitmapFactory.decodeFile(catchFile.getAbsolutePath());
                imageView.setImageBitmap(BitmapFactory.decodeFile(catchFile.getAbsolutePath()));
                //imageView.setImageBitmap(BitmapScaler.scaleToFitHeight(BitmapFactory.decodeFile(catchFile.getAbsolutePath()), getScreenHeight(getActivity())-(p24+p25+p26+10)));
                container.addView(imageView, 0);

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent intent = new Intent(getActivity(), FullScreenImage.class);
                            intent.putExtra("ImageArray", _imagesPath);
                            intent.putExtra("Position", position);
                            startActivity(intent);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }

        public CharSequence getPageTitle(int position) {
            return "Your static title";
        }
    }*/

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onPageSelected(int position) {
        // TODO Auto-generated method stub
        Log.d("###onPageSelected, pos ", String.valueOf(position));
        try {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselected_itemdots));
            }
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dots));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    private void setUiPageViewController() {
        try {
            dotsCount = mAdapter.getCount();
            dots = new ImageView[dotsCount];

            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselected_itemdots));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                pager_indicator.addView(dots[i], params);
            }

            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dots));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
