package com.itravelapp.damdama.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itravelapp.damdama.R;
import com.itravelapp.damdama.db.MVCDBController;
import com.itravelapp.damdama.models.TreeListModel;
import com.itravelapp.damdama.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntrestingTriviaFragment extends Fragment {

    protected TextView tvBotanicalName;
    protected TextView tvLocalTitle;
    protected TextView tvBotanicalTitle;
    protected TextView tvTriviaTitle;
    TreeListModel treeListModel = new TreeListModel();
    protected View rootView;
    protected TextView tvLocalName;
    protected TextView tvInterestinTrivia;
    protected Bundle bundle;
    protected MVCDBController mvcdbController;

    public IntrestingTriviaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_intresting_trivia, container, false);
        initView(rootView);
        bundle = getArguments();
        mvcdbController = new MVCDBController(getActivity());
        treeListModel = mvcdbController.getTreeTriviaFromId(bundle.getString("TreeID"));

        if (Utility.isNotNullNotEmptyNotWhiteSpace(treeListModel.getLocalName()) && !treeListModel.getLocalName().equals("0")) {
            tvLocalTitle.setVisibility(View.VISIBLE);
            tvLocalName.setVisibility(View.VISIBLE);
            tvLocalName.setText(treeListModel.getLocalName());
        }
        if (Utility.isNotNullNotEmptyNotWhiteSpace(treeListModel.getBotanicalName()) && !treeListModel.getBotanicalName().equals("0")) {
            tvBotanicalTitle.setVisibility(View.VISIBLE);
            tvBotanicalName.setVisibility(View.VISIBLE);
            tvBotanicalName.setText(treeListModel.getBotanicalName());
        }

        if (Utility.isNotNullNotEmptyNotWhiteSpace(treeListModel.getWhichYouLike())) {
            tvTriviaTitle.setVisibility(View.VISIBLE);
            tvInterestinTrivia.setVisibility(View.VISIBLE);
            tvInterestinTrivia.setText(treeListModel.getWhichYouLike());
        }

        return rootView;
    }

    private void initView(View rootView) {
        tvLocalName = (TextView) rootView.findViewById(R.id.tv_localName);
        tvInterestinTrivia = (TextView) rootView.findViewById(R.id.tv_interestinTrivia);
        tvBotanicalName = (TextView) rootView.findViewById(R.id.tv_botanicalName);
        tvLocalTitle = (TextView) rootView.findViewById(R.id.tv_localTitle);
        tvBotanicalTitle = (TextView) rootView.findViewById(R.id.tv_botanicalTitle);
        tvTriviaTitle = (TextView) rootView.findViewById(R.id.tv_triviaTitle);
    }
}
